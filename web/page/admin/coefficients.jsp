<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><fmt:message key="coefficients"/></title>
  <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
  <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<nav class="navbar">
  <div class="container-fluid">
    <div>
      <ul class="nav navbar-nav" role="tablist">
        <li><a href="\races"><fmt:message key="active.races"/></a></li>
        <li><a href="\creation_race"><fmt:message key="create.race"/></a></li>
        <li><a href="\coefficients?id=${id}&command=log_out"><fmt:message key="log.out"/></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="\coefficients?id=${id}&command=change_language&language=ru_ru">RU</a></li>
        <li><a href="\coefficients?id=${id}&command=change_language&language=en_us">EN</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="details">
  <form>
    <table class="table table-bordered">
      <thead>
      <tr>
        <th><fmt:message key="horse.name"/></th>
        <th><fmt:message key="win"/></th>
        <th><fmt:message key="place"/></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach begin="0" end="${list.size() / 2 - 1}" var="index">
        <tr>
          <td>${list[index*2].horseName}</td>
          <td class="clear-fix">
              ${list[index*2].coefficient}
          </td>
          <td class="clear-fix">
              ${list[index*2+1].coefficient}
          </td>
        </tr>
      </c:forEach>
      </tbody>
      <tfoot>
      </tfoot>
    </table>
  </form>
</div>
</body>
</html>