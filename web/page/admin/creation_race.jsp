<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language.toString()}"/>
<fmt:setBundle basename="property.jsptext"/>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="create.race"/></title>
    <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/page/js/creation_race.js"></script>
    <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<c:set var="closeText"><fmt:message key="close"/></c:set>
<ctg:messageWindow message="${message}" typeMessage="${type_message}" closeText="${closeText}"/>
<nav class="navbar">
    <div class="container-fluid">
        <div>
            <ul class="nav navbar-nav" role="tablist">
                <li><a href="${pageContext.request.contextPath}/races"><fmt:message key="active.races"/></a></li>
                <li><a href="${pageContext.request.contextPath}/races?command=log_out"><fmt:message key="log.out"/></a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="\creation_race?command=change_language&language=ru_ru">RU</a></li>
                <li><a href="\creation_race?command=change_language&language=en_us">EN</a></li>
            </ul>
        </div>
    </div>
</nav>
<br/>

<div class="create-race-wrapper">
    <div class="buttons clear-fix">
        <button id="add-button" class="btn btn-primary"><fmt:message key="add.horse"/></button>
        <button id="delete-button" class="btn btn-primary"><fmt:message key="delete.horse"/></button>
    </div>
    <form id="creation-form" action="${pageContext.request.contextPath}/ServletController">
        <input type="hidden" name="command" value="create_race">
        <input type="hidden" name="page" value="creation_race"/>
        <input type="hidden" id="count-field" name="count_horses" value="4">

        <div class="input-wrapper clear-fix">
            <label for="race-name"><fmt:message key="name.race"/></label>
            <input id="race-name" name="name_race" type="text" class="form-control" pattern=".{1,25}"
                   title="<fmt:message key="hint.format.name.race"/>" required>
        </div>
        <div class="input-wrapper clear-fix">
            <label for="date"><fmt:message key="time.starting"/></label>
            <input id="date" name="time_starting" type="datetime-local" class="form-control" required>
        </div>
        <div class="input-wrapper clear-fix">
            <label for="horse-name1"><fmt:message key="horse.name"/> No.1</label>
            <input id="horse-name1" name="horse_name1" type="text" class="form-control" pattern=".{1,25}" required>
        </div>
        <div class="input-wrapper clear-fix">
            <label for="horse-name2"><fmt:message key="horse.name"/> No.2</label>
            <input id="horse-name2" name="horse_name2" type="text" class="form-control" pattern=".{1,25}" required>
        </div>
        <div class="input-wrapper clear-fix">
            <label for="horse-name3"><fmt:message key="horse.name"/> No.3</label>
            <input id="horse-name3" name="horse_name3" type="text" class="form-control" pattern=".{1,25}" required>
        </div>
        <div class="input-wrapper clear-fix">
            <label for="horse-name4"><fmt:message key="horse.name"/> No.4</label>
            <input id="horse-name4" name="horse_name4" type="text" class="form-control" pattern=".{1,25}" required>
        </div>
        <input id="submit-button" type="submit" class="btn btn-primary" value="<fmt:message key="submit"/>">
    </form>
</div>
</body>
</html>