<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><fmt:message key="set.results"/></title>
  <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
  <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<nav class="navbar">
  <div class="container-fluid">
    <div>
      <ul class="nav navbar-nav" role="tablist">
        <li><a href="\races"><fmt:message key="active.races"/></a></li>
        <li><a href="\balance"><fmt:message key="balance"/></a></li>
        <li><a href="\results?id=${id}&command=log_out"><fmt:message key="log.out"/></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="\results?id=${id}&command=change_language&language=ru_ru">RU</a></li>
        <li><a href="\results?id=${id}&command=change_language&language=en_us">EN</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="make-results-wrapper">
  <h2><fmt:message key="your.won"/> ${amountWin} $</h2>
  <table class="table table-bordered">
    <thead>
    <tr>
      <th><fmt:message key="horse.name"/></th>
      <th><fmt:message key="position"/></th>
      <th><fmt:message key="win.coefficient"/></th>
      <th><fmt:message key="win.bet"/></th>
      <th><fmt:message key="place.coefficient"/></th>
      <th><fmt:message key="place.bet"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="element" items="${list}">
    <tr>
      <td>${element.horseName}</td>
      <td>${element.position}</td>
      <td>${element.winCoefficient}</td>
      <td>${element.winBet}</td>
      <td>${element.placeCoefficient}</td>
      <td>${element.placeBet}</td>
    </tr>
    </c:forEach>
    </tbody>
    <tfoot>
    </tfoot>
  </table>
</div>
</body>
</html>