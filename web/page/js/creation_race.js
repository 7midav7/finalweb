var count=4;
var MAX_SIZE = 15;
var MIN_SIZE = 0;
var HORSE_NAME;

document.addEventListener("DOMContentLoaded", function addListener(){

    var positionFirstInput = 2;
    HORSE_NAME = document.getElementsByClassName("input-wrapper").item(positionFirstInput).textContent;
    HORSE_NAME = HORSE_NAME.replace(" No.1", "");

    document.getElementById("delete-button").onclick = function deleteLastInputField(){
        document.getElementsByClassName("input-wrapper").item(count + 1).remove();
        document.getElementById("count-field").setAttribute("value", count.toString());
        --count;
        if (count == MIN_SIZE){
            document.getElementById("delete-button").style.visibility = "hidden";
        }
        if (count == MIN_SIZE + 3){
            document.getElementById("submit-button").style.visibility = "hidden";
        }
        if (count == MAX_SIZE - 1){
            document.getElementById("add-button").style.visibility = "visible";
        }
    };

    document.getElementById("add-button").onclick = function addInputField(){
        ++ count;
        var div = document.createElement("div");
        var input = document.createElement("input");
        var label = document.createElement("label");
        div.className="input-wrapper clear-fix";
        input.setAttribute("name", "horse" + count);
        input.setAttribute("id", "horse-name" + count);
        input.setAttribute("name", "horse_name" + count);
        input.setAttribute("pattern", ".{1,25}");
        input.setAttribute("required", "true");
        input.className="form-control";
        label.setAttribute("value","Name horse No." + count);
        label.setAttribute("for", "horse-name" + count);
        label.textContent = HORSE_NAME + " No." + count;
        document.getElementById("count-field").setAttribute("value", count.toString());
        document.getElementById("creation-form").insertBefore(div, document.getElementById("submit-button"));
        document.getElementsByClassName("input-wrapper").item(count + 1).appendChild(label);
        document.getElementsByClassName("input-wrapper").item(count + 1).appendChild(input);

        if (count == MIN_SIZE + 1){
            document.getElementById("delete-button").style.visibility = "visible";
        }
        if (count == MIN_SIZE + 4){
            document.getElementById("submit-button").style.visibility = "visible";
        }
        if (count == MAX_SIZE){
            document.getElementById("add-button").style.visibility = "hidden";
        }
    };
});