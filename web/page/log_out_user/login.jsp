<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
    <title><fmt:message key="log.in"/> </title>
</head>
<body>
<ul class="nav nav-pills" role="tablist">
    <li><a href="\log_in?command=change_language&language=ru_ru">RU</a></li>
    <li><a href="\log_in?command=change_language&language=en_us">EN</a></li>
</ul>
<c:set var="closeText"><fmt:message key="close"/></c:set>
<ctg:messageWindow message="${message}" typeMessage="${type_message}" closeText="${closeText}"/>
<h1 class="font-welcome-text"><fmt:message key="greeting.log.in.page"/></h1>
<div class="image-cover-home">
    <form class="login-form-wrapper" method="post" action="${pageContext.request.contextPath}/ServletController">
        <input type="hidden" name="command" value="log_in">
        <input type="hidden" name="page" value="log_in">

        <div class="login-form">
            <input type="text" class="form-control" name="username" placeholder="<fmt:message key="username"/>">

            <div class="second-line">
                <input type="password" class="form-control" name="password" placeholder="<fmt:message key="password"/>">
                <input type="submit" class="btn-primary" value="<fmt:message key="log.in"/>">
            </div>
            <a href="${pageContext.request.contextPath}/registration"><fmt:message key="registration"/></a>
        </div>
    </form>
</div>
</body>
</html>