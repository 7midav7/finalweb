<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Notifications</title>
    <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<nav class="navigation">
    <a href="${pageContext.request.contextPath}/page/user/home.jsp">
        <img alt="Home" src="${pageContext.request.contextPath}/page/img/home.png" width="40" height="40">
        Home
    </a>
    <div>Cash: 500$;</div>
    <a href="${pageContext.request.contextPath}/page/user/notification.jsp">
        Notifications
        <img alt="Notification" src="${pageContext.request.contextPath}/page/img/notification.png" width="40" height="40">
    </a>
</nav>
<div class="notification-wrapper">
    <div class="message">
        Message1 sdkfj sdlfkjs df lskdjf l sdfkj lsdkf sldkjf owieu rsld df
    </div>
    <div class="message">
        Message1 sdkfj sdlfkjs df lskdjf l sdfkj lsdkf sldkjf owieu rsld df
    </div>
    <div class="message">
        Message1 sdkfj sdlfkjs df lskdjf l sdfkj lsdkf sldkjf owieu rsld df
    </div>
</div>
</body>
</html>