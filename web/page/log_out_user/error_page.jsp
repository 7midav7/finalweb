<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="RU_RU" />
<fmt:setBundle basename="property.jsptext" var="ruBundle"/>
<fmt:setLocale value="EN_US" />
<fmt:setBundle basename="property.jsptext" var="enBundle"/>
<html>

<head>
  <title><fmt:message key="error" bundle="${enBundle}"/></title>
  <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
  <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<div class="image-cover-error-page">
  <div class="error-page-wrapper">
    <h2><a href="\races"><fmt:message key="sorry.something.went.wrong" bundle="${ruBundle}"/></a></h2>
    <h2><a href="\races"><fmt:message key="sorry.something.went.wrong" bundle="${enBundle}"/></a></h2>
  </div>
</div>
</body>
</html>