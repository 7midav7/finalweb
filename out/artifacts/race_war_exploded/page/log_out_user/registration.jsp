<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<html>

<head>
    <title><fmt:message key="registration"/></title>
    <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<c:set var="closeText"><fmt:message key="close"/></c:set>
<ctg:messageWindow message="${message}" typeMessage="${type_message}" closeText="${closeText}"/>
<nav class="navbar">
    <div class="container-fluid">
        <div>
            <ul class="nav navbar-nav" role="tablist">
                <li><a href="\log_in"><fmt:message key="log.in"/></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="\registration?command=change_language&language=ru_ru">RU</a></li>
                <li><a href="\registration?command=change_language&language=en_us">EN</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="image-cover-register">
    <br><!-- bad practise , problem with positioning //TODO: fix it -->
    <div class="register-form-wrapper">
        <form class="register-form" name="registration" method="post" action="${pageContext.request.contextPath}/ServletController">
            <input type="hidden" name="command" value="registration">
            <input type="hidden" name="page" value="registration">
            <div>
                <label for="label-rg1"><fmt:message key="username"/> *</label>
                <input type="text" name="username" id="label-rg1" class="form-control" required
                       pattern="([\w]){1,12}" title="<fmt:message key='hint.format.username'/>" >
            </div>
            <div>
                <label for="label-rg3"><fmt:message key="password"/> *</label>
                <input type="password" name="password" id="label-rg3" class="form-control" required
                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w]{8,20}"
                       onchange="document.getElementById('label-rg4').setAttribute('pattern', this.value)"
                       title="<fmt:message key="hint.format.password"/> "/>
            </div>
            <div>
                <label for="label-rg4"><fmt:message key="repeat.password"/>*</label>
                <input type="password" name="duplicate-password" id="label-rg4" class="form-control" required
                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w]{8,20}"
                       title="<fmt:message key="hint.format.double.password"/> "/>
            </div>
            <div>
                <label for="label-rg5"><fmt:message key="email"/>*</label>
                <input type="email" name="email" id="label-rg5" class="form-control" required
                       pattern="[\w\.-]+@[a-zA-Z\d-]+\.[a-zA-Z\d-\.]+">
            </div>
            <div>
                <label for="label-rg6"><fmt:message key="phone"/>*</label>
                <input type="text" name="phone" id="label-rg6" class="form-control"
                       required pattern="\+?\(?\d{2,4}\)?[\d\s-]{3,}">
            </div>
            <div>
                <input type="submit" class="btn-primary" value="<fmt:message key="registration"/> ">
            </div>
        </form>
    </div>
</div>
</body>
</html>