<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Result race</title>
    <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<nav class="navigation">
    <a href="${pageContext.request.contextPath}/page/user/home.jsp">
        <img alt="Home" src="${pageContext.request.contextPath}/page/img/home.png" width="40" height="40">
        Home
    </a>
    <div>Cash: 500$;</div>
    <a href="${pageContext.request.contextPath}/page/user/notification.jsp">
        Notifications
        <img alt="Notification" src="${pageContext.request.contextPath}/page/img/notification.png" width="40" height="40">
    </a>
</nav>
<div class="table-wrapper">
    <table class="table-bordered table">
        <thead>
        <tr>
            <th>Name Horse</th>
            <th>Place</th>
            <th>Coefficients</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Horse 1</td>
            <td>1</td>
            <td>1:5</td>
        </tr>
        <tr>
            <td>Horse 2</td>
            <td>2</td>
            <td>2:19</td>
        </tr>
        <tr>
            <td>Horse 3</td>
            <td>3</td>
            <td>2:8</td>
        </tr>
        <tr>
            <td>Horse 4</td>
            <td>4</td>
            <td>9:10</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="description">
    <h2>Description of race</h2>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis a euismod neque. Nulla faucibus vehicula blandit. Integer maximus urna ut nulla lobortis, vitae sagittis augue aliquet. Nulla viverra nisi id augue ullamcorper fermentum. Donec eleifend lacus sed convallis efficitur. Donec quis turpis nec justo facilisis convallis. Morbi risus urna, consequat at imperdiet eget, rutrum et dolor. Cras consequat facilisis ligula sed pretium. Donec faucibus est vitae ex fermentum, eget facilisis libero luctus.
    </p>
</div>
</body>
</html>