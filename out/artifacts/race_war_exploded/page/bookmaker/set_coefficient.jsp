<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><fmt:message key="race.details"/></title>
  <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
  <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<c:set var="closeText"><fmt:message key="close"/></c:set>
<ctg:messageWindow message="${message}" typeMessage="${type_message}" closeText="${closeText}"/>
<nav class="navbar">
  <div class="container-fluid">
    <div>
      <ul class="nav navbar-nav" role="tablist">
        <li><a href="\races"><fmt:message key="active.races"/></a></li>
        <li><a href="\races?id=${id}&command=log_out"><fmt:message key="log.out"/></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="\set_coefficients?id=${id}&command=change_language&language=ru_ru">RU</a></li>
        <li><a href="\set_coefficients?id=${id}&command=change_language&language=en_us">EN</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="set-coefficients">
  <form method="post" action="ServletController">
    <input type="hidden" name="command" value="set_coefficients">
    <input type="hidden" name="page" value="set_coefficients">
    <input type="hidden" name="id" value="${id}">
    <input type="hidden" name="count" value="${list.size()}">
    <table class="table table-bordered">
      <thead>
      <tr>
        <th><fmt:message key="horse.name"/></th>
        <th><fmt:message key="win"/></th>
        <th><fmt:message key="place"/></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${list}" var="element" varStatus="loop">
        <input type="hidden" name="horse-id-${loop.index}" value="${element.id}">
        <tr>
          <td>${element.name}</td>
          <td class="clear-fix">
            <input class="form-control" type="text" name="win-coefficient-${loop.index}" pattern="([1-9]|(1[0-9]))(\.\d{1,2})?" required>
          </td>
          <td class="clear-fix">
            <input class="form-control" type="text" name="place-coefficient-${loop.index}" pattern="([1-9]|(1[0-9]))(\.\d{1,2})?" required>
          </td>
        </tr>
      </c:forEach>
      </tbody>
      <tfoot>
      <tr>
        <td colspan="4"><input type="submit" class="btn-primary" value="<fmt:message key="set.coefficients"/>"></td>
      </tr>
      </tfoot>
    </table>
  </form>
</div>
</body>
</html>