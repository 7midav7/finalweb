<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><fmt:message key="race.details"/></title>
  <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
  <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<c:set var="closeText"><fmt:message key="close"/></c:set>
<ctg:messageWindow message="${message}" typeMessage="${type_message}" closeText="${closeText}"/>
<nav class="navbar">
  <div class="container-fluid">
    <div>
      <ul class="nav navbar-nav" role="tablist">
        <li><a href="\races"><fmt:message key="active.races"/></a></li>
        <li><a href="\races?command=log_out"><fmt:message key="log.out"/></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="\balance?command=change_language&language=ru_ru">RU</a></li>
        <li><a href="\balance?command=change_language&language=en_us">EN</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="balance-wrapper">
  <div class="balance-table">
    <h2><fmt:message key="your.current.balance"/>: ${cash}$</h2>
    <form method="post" action="balance">
      <input type="hidden" name="command" value="transfer_money"/>
      <input type="hidden" name="page" value="balance"/>
      <div class="clear-fix">
        <label for="amount"><fmt:message key="enter.amount"/>:</label>
        <input id="amount" class="form-control" type="text"
               name="amount" pattern="\d{1,9}(\.\d{1,2})?" title="<fmt:message key="hint.format.amount.dollar"/>" required>
      </div>
      <div class="clear-fix">
        <label for="account_id"><fmt:message key="enter.id.bank.account"/>:</label>
        <input id="account_id" class="form-control" type="text"
               name="account_id" pattern="\d{5}" title="<fmt:message key="hint.format.id.bank.account"/>" required/>
      </div>
      <input type="submit" class="btn-primary" value="<fmt:message key="transfer.money"/>">
    </form>
  </div>
</div>
</body>
</html>