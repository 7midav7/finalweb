<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="coefficients"/></title>
    <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<c:set var="closeText"><fmt:message key="close"/></c:set>
<ctg:messageWindow message="${message}" typeMessage="${type_message}" closeText="${closeText}"/>
<nav class="navbar">
    <div class="container-fluid">
        <div>
            <ul class="nav navbar-nav" role="tablist">
                <li><a href="\races"><fmt:message key="active.races"/></a></li>
                <li><a href="\balance"><fmt:message key="balance"/></a></li>
                <li><a href="\coefficients?id=${id}&command=log_out"><fmt:message key="log.out"/></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="\coefficients?id=${id}&command=change_language&language=ru_ru">RU</a></li>
                <li><a href="\coefficients?id=${id}&command=change_language&language=en_us">EN</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="coefficients">
    <h2><fmt:message key="your.current.balance"/>: ${cash}$</h2>
    <c:if test="${not empty listBets}">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th><fmt:message key="horse.name"/></th>
                <th><fmt:message key="win.coefficient"/></th>
                <th><fmt:message key="win.bet"/></th>
                <th><fmt:message key="place.coefficient"/></th>
                <th><fmt:message key="place.bet"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach begin="0" end="${listBets.size() / 2 - 1}" var="index">
                <tr>
                    <td>${listBets[index*2].coefficient.horseName}</td>
                    <td>${listBets[index*2].coefficient.coefficient}</td>
                    <td>${listBets[index*2].bet.amount}</td>
                    <td>${listBets[index*2+1].coefficient.coefficient}</td>
                    <td>${listBets[index*2+1].bet.amount}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <c:if test="${not empty list}">
    <form>
        <input type="hidden" name="command" value="bet"/>
        <input type="hidden" name="page" value="coefficients"/>
        <input type="hidden" name="id" value="${id}"/>
        <input type="hidden" name="count" value="${list.size()}"/>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th><fmt:message key="horse.name"/></th>
                <th><fmt:message key="win"/></th>
                <th><fmt:message key="place"/></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach begin="0" end="${list.size() / 2 - 1}" var="index">
                    <tr>
                        <td>${list[index*2].horseName}</td>
                        <td class="clear-fix">
                            <span>${list[index*2].coefficient}</span>
                            <input type="hidden" name="id-coefficient-${index*2}" value="${list[index*2].id}">
                            <input class="form-control" type="text" pattern="\d{1,9}"
                                   name="bet-${index*2}" value="0" required>
                        </td>
                        <td class="clear-fix">
                            <span>${list[index*2+1].coefficient}</span>
                            <input type="hidden" name="id-coefficient-${index*2 + 1}" value="${list[index*2 + 1].id}">
                            <input class="form-control" type="text" pattern="\d{1,9}"
                                   name="bet-${index*2 + 1}" value="0" required>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4"><input type="submit" class="btn btn-primary" value="<fmt:message key="submit"/>"></td>
            </tr>
            </tfoot>
        </table>
    </form>
    </c:if>
</div>
</body>
</html>