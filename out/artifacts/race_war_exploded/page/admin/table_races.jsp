<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><fmt:message key="races"/></title>
  <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
  <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<nav class="navbar">
  <div class="container-fluid">
    <div>
      <ul class="nav navbar-nav" role="tablist">
        <li><a href="${pageContext.request.contextPath}/creation_race"><fmt:message key="create.race"/></a></li>
        <li><a href="${pageContext.request.contextPath}/races?command=log_out"><fmt:message key="log.out"/></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="\races?command=change_language&language=ru_ru">RU</a></li>
        <li><a href="\races?command=change_language&language=en_us">EN</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="race-table-wrapper">
  <table class="table table-bordered">
    <thead>
    <tr>
      <th><fmt:message key="name.race"/></th>
      <th><fmt:message key="time.starting"/></th>
      <th><fmt:message key="number.horses"/></th>
      <th><fmt:message key="status"/></th>
      <th><fmt:message key="coefficients"/></th>
      <th><fmt:message key="results"/></th>
      <th><fmt:message key="action"/></th>
    </tr>
    </thead>
    <tbody>
      <c:forEach var="element" items="${list}">
        <tr>
          <td>${element.nameRace}</td>
          <td>${element.dateStarting}</td>
          <td>${element.countHorses}</td>
          <td>
            <c:if test="${element.stage == 'WAITING_COEFFICIENTS'}">
              <fmt:message key="waiting.coefficients.stage"/>
            </c:if>
            <c:if test="${element.stage == 'WAITING_RESULTS'}">
              <fmt:message key="waiting.results.stage"/>
            </c:if>
            <c:if test="${element.stage == 'FINAL_STAGE'}">
              <fmt:message key="final.stage"/>
            </c:if>
          </td>
          <td><c:if test="${element.stage != 'WAITING_COEFFICIENTS'}">
            <a href="/coefficients?id=${element.id}"><fmt:message key="read.more"/></a>
          </c:if></td>
          <td>
            <c:if test="${element.stage == 'FINAL_STAGE'}">
              <a href="/results?id=${element.id}"><fmt:message key="read.more"/></a>
            </c:if>
            <c:if test="${element.stage == 'WAITING_RESULTS'}">
              <a href="/set_results?id=${element.id}"><fmt:message key="set.results"/></a>
            </c:if>
          </td>
          <td><form action="ServletController" method="post">
            <input type="hidden" name="command" value="delete_race">
            <input type="hidden" name="page" value="races">
            <input type="hidden" name="id" value="${element.id}">
            <button class="btn btn-primary"><fmt:message key="delete"/></button>
          </form>
          </td>
        </tr>
      </c:forEach>
    </tbody>
  </table>
</div>
</body>
</html>