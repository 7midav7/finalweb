<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.language.toString()}" />
<fmt:setBundle basename="property.jsptext"/>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="set.results"/></title>
    <script src="${pageContext.request.contextPath}/page/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/page/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/page/css/style.css">
</head>
<body>
<c:set var="closeText"><fmt:message key="close"/></c:set>
<ctg:messageWindow message="${message}" typeMessage="${type_message}" closeText="${closeText}"/>
<nav class="navbar">
    <div class="container-fluid">
        <div>
            <ul class="nav navbar-nav" role="tablist">
                <li><a href="\races"><fmt:message key="active.races"/></a></li>
                <li><a href="\creation_race"><fmt:message key="create.race"/></a></li>
                <li><a href="\races?id=${id}&command=log_out"><fmt:message key="log.out"/></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="\set_results?id=${id}&command=change_language&language=ru_ru">RU</a></li>
                <li><a href="\set_results?id=${id}&command=change_language&language=en_us">EN</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="make-results-wrapper">
    <form action="ServletController">
        <input type="hidden" name="command" value="set_results">
        <input type="hidden" name="page" value="set_results">
        <input type="hidden" name="count" value="${list.size()}">
        <input type="hidden" name="id" value="${id}">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th><fmt:message key="horse.name"/></th>
                <th><fmt:message key="position"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="element" items="${list}" varStatus="loop">
                <tr>
                    <td>${element.name}</td>
                    <td>
                        <input type="hidden" name="horse-id-${loop.index}" value="${element.id}">
                        <input class="form-control" type="text" name="position-${loop.index}" pattern="\d{0,9}">
                    </td>
                </tr>
            </c:forEach>
            <tfoot>
            <tr>
                <td colspan="2">
                    <input type="submit" value="<fmt:message key="set.results"/>" class="btn btn-primary">
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>
</body>
</html>