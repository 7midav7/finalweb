package com.marchenko.race.domain;

import java.io.Serializable;

/**
 * Created by Marchenko Vadim on 11/9/2015.
 */
public interface Domain extends Serializable, Cloneable {
}
