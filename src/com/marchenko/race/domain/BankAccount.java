package com.marchenko.race.domain;

/**
 * Created by Marchenko Vadim on 12/16/2015.
 */
public class BankAccount implements Domain{
    private int id;
    private int cash;
    private String name;

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
