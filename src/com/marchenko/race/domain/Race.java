package com.marchenko.race.domain;

import java.time.LocalDateTime;

/**
 * Created by Marchenko Vadim on 11/30/2015.
 */
public class Race implements Domain {
    public enum Stage{WAITING_COEFFICIENTS, WAITING_RESULTS, FINAL_STAGE}

    private int id;
    private String nameRace;
    private LocalDateTime timeStarting;
    private int countHorses;
    private String[] array;
    private Stage stage;

    public int getId() {
        return id;
    }

    public String getNameRace() {
        return nameRace;
    }

    public LocalDateTime getTimeStarting() {
        return timeStarting;
    }

    public int getCountHorses() {
        return countHorses;
    }

    public String[] getArray() {
        return array;
    }

    public Stage getStage() {
        return stage;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNameRace(String nameRace) {
        this.nameRace = nameRace;
    }

    public void setTimeStarting(LocalDateTime timeStarting) {
        this.timeStarting = timeStarting;
    }

    public void setCountHorses(int countHorses) {
        this.countHorses = countHorses;
    }

    public void setArray(String[] array) {
        this.array = array;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
