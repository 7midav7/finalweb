package com.marchenko.race.domain;

/**
 * Created by Marchenko Vadim on 12/1/2015.
 */
public class Horse implements Domain {
    private int id;
    private String name;
    private int raceId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRaceId() {
        return raceId;
    }

    public void setRaceId(int raceId) {
        this.raceId = raceId;
    }
}
