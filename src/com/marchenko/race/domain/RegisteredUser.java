package com.marchenko.race.domain;

/**
 * Created by Marchenko Vadim on 11/24/2015.
 */
public class RegisteredUser extends User{
    private String email;
    private String phone;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
