package com.marchenko.race.domain;

/**
 * Created by Marchenko Vadim on 11/9/2015.
 */
public class User implements Domain {
    public enum Role{ADMIN, BOOKMAKER, USER, LOG_OUT_USER }

    private int id;
    private double cash;
    private String username;
    private Role role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
