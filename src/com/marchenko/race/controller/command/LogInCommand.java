package com.marchenko.race.controller.command;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.domain.RegisteredUser;
import com.marchenko.race.service.ServiceException;
import com.marchenko.race.service.RegisteredUserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/9/2015.
 */

// TODO: refactor on Parameters!!!!
public class LogInCommand implements Command {
    private static final String PARAM_NAME_USERNAME = "username";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String MESSAGE_INVALID_LOGIN = "wrong.username.password";
    private static final String URL_NEW_START_PAGE = "/races";

    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        String username = request.getParameter(PARAM_NAME_USERNAME);
        String password = request.getParameter(PARAM_NAME_PASSWORD);
        try {
            RegisteredUserService service = RegisteredUserService.getInstance();
            Optional<RegisteredUser> userOptional =
                    service.findUserDatabaseByUsernamePassword(username, password);
            if (userOptional.isPresent()) {
                SessionAction.getInstance().logInUser(request, userOptional.get());
                return Optional.of(URL_NEW_START_PAGE);
            }
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
        MessageAction.getInstance().transferErrorMessage(request, MESSAGE_INVALID_LOGIN);
        return Optional.empty();
    }
}
