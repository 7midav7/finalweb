package com.marchenko.race.controller.command;

import com.marchenko.race.controller.SessionAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/9/2015.
 */
public class LogOutCommand implements Command {
    @Override
    public Optional<String> execute(HttpServletRequest request) {
        SessionAction.getInstance().logOutUser(request);
        return Optional.empty();
    }
}
