package com.marchenko.race.controller.command;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.controller.parameter.RegistrationParameter;
import com.marchenko.race.controller.validation.RegistrationValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.domain.RegisteredUser;
import com.marchenko.race.domain.User;
import com.marchenko.race.service.ServiceException;
import com.marchenko.race.service.RegisteredUserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/9/2015.
 */
public class RegistrationCommand implements Command {
    private static final String URL_NEW_START_PAGE = "/races";

    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        RegistrationParameter parameters = new RegistrationParameter();
        parameters.initialize(request);
        Validator<RegistrationParameter> validator = RegistrationValidator.getInstance();
        Optional<String> message = validator.receiveMistakeMessageIfExist(parameters);

        Optional<String> forwardPage;
        if (!message.isPresent()) {
                registration(parameters);
                addUserToSession(request, parameters);
                forwardPage = Optional.of(URL_NEW_START_PAGE);
        } else {
            MessageAction.getInstance().transferErrorMessage(request, message.get());
            forwardPage = Optional.empty();
        }

        return forwardPage;
    }

    private void registration(RegistrationParameter parameters) throws ControllerException{
        RegisteredUserService service = RegisteredUserService.getInstance();
        try{
            service.registration(parameters);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }

    private void addUserToSession(HttpServletRequest request, RegistrationParameter parameters) throws ControllerException{
        RegisteredUserService service = RegisteredUserService.getInstance();
        Optional<RegisteredUser> userOptional;
        try {
            userOptional =
                    service.findUserDatabaseByUsernamePassword(parameters.getUsername(),
                            parameters.getPassword());
        } catch (ServiceException e){
            throw new ControllerException(e);
        }
        User user = userOptional.get();
        if (userOptional.isPresent()) {
            SessionAction.getInstance().logInUser(request, user);
        }
    }
}
