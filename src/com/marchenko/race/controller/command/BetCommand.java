package com.marchenko.race.controller.command;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.controller.parameter.BetParameter;
import com.marchenko.race.controller.validation.BetValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.domain.Bet;
import com.marchenko.race.service.BetService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class BetCommand implements Command {
    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        BetParameter parameter = new BetParameter();
        parameter.initialize(request);
        Validator<BetParameter> validator = BetValidator.getInstance();
        Optional<String> message = validator.receiveMistakeMessageIfExist(parameter);

        if (!message.isPresent()){
            List<Bet> bets = parameter.transferDomain();
            Optional<Integer> userIdOptional = SessionAction.getInstance().takeUserId(request);
            int userId = userIdOptional.get();
            try {
                BetService.getInstance().makeBets(bets, userId);
            } catch (ServiceException e) {
                throw new ControllerException(e);
            }
            MessageAction.getInstance().transferSuccessfulOperationMessage(request);
        } else {
            MessageAction.getInstance().transferErrorMessage(request, message.get());
        }

        return Optional.empty();
    }
}
