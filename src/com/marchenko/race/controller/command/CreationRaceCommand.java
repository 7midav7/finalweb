package com.marchenko.race.controller.command;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.controller.parameter.CreationRaceParameter;
import com.marchenko.race.controller.validation.CreationRaceValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.service.RaceService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/28/2015.
 */
public class CreationRaceCommand implements Command {

    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {

        CreationRaceParameter parameters = new CreationRaceParameter();
        parameters.initialize(request);
        Validator<CreationRaceParameter> validator = CreationRaceValidator.getInstance();
        Optional<String> optional = validator.receiveMistakeMessageIfExist(parameters);

        if (!optional.isPresent()){
            RaceService service = RaceService.getInstance();
            try {
                service.addRace(parameters);
            } catch (ServiceException e) {
                throw new ControllerException(e);
            }
            MessageAction.getInstance().transferSuccessfulOperationMessage(request);
        } else {
            String message = optional.get();
            MessageAction.getInstance().transferErrorMessage(request, message);
        }

        return Optional.empty();
    }
}
