package com.marchenko.race.controller.command;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.controller.parameter.TransferMoneyParameter;
import com.marchenko.race.controller.validation.TransferMoneyValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.service.BankService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/16/2015.
 */

//TODO: refactor parseInt, incapsulate initialization
public class TransferFromBankCommand implements Command {


    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        TransferMoneyParameter parameter = new TransferMoneyParameter();
        parameter.initialize(request);
        Validator<TransferMoneyParameter> validator = TransferMoneyValidator.getInstance();
        Optional<String> optional = validator.receiveMistakeMessageIfExist(parameter);

        if (!optional.isPresent()){
            if (transferMoney(request, parameter)){
                MessageAction.getInstance().transferSuccessfulOperationMessage(request);
            } else {
                MessageAction.getInstance().transferCrushedOperationErrorMessage(request);
            }
        } else {
            String message = optional.get();
            MessageAction.getInstance().transferErrorMessage(request, message);
        }

        return Optional.empty();
    }

    private boolean transferMoney(HttpServletRequest request, TransferMoneyParameter parameter) throws ControllerException {
        BankService service = BankService.getInstance();
        String nameAccount = parameter.getAccountName();
        double amount = parameter.getAmount();
        Optional<Integer> idUserOptional = SessionAction.getInstance().takeUserId(request);
        int idUser = idUserOptional.get();
        try {
            return service.transferBankUser(nameAccount, amount, idUser);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }
}
