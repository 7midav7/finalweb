package com.marchenko.race.controller.command;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.controller.parameter.DeleteRaceParameter;
import com.marchenko.race.controller.validation.DeleteRaceValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.service.RaceService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/4/2015.
 */
public class DeleteRaceCommand implements Command {

    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        DeleteRaceParameter parameters = new DeleteRaceParameter();
        parameters.initialize(request);
        Validator<DeleteRaceParameter> validator = DeleteRaceValidator.getInstance();
        Optional<String> message = validator.receiveMistakeMessageIfExist(parameters);

        if (!message.isPresent()){
            RaceService service = RaceService.getInstance();
            try {
                service.deleteRaceById(parameters.getId());
                MessageAction.getInstance().transferSuccessfulOperationMessage(request);
            } catch (ServiceException e) {
                throw new ControllerException(e);
            }
        } else {
            String mes = message.get();
            MessageAction.getInstance().transferErrorMessage(request, mes);
        }
        return Optional.empty();
    }
}
