package com.marchenko.race.controller.command;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.LocalizationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/10/2015.
 */
public class ChangeLanguageCommand implements Command {
    private static final String PARAMETER_NAME_LANGUAGE = "language";

    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        String language = request.getParameter(PARAMETER_NAME_LANGUAGE).toUpperCase();
        LocalizationManager.TypeLocalization type = LocalizationManager.TypeLocalization.valueOf(language);
        if (type == null){
            type = LocalizationManager.TypeLocalization.EN_US;
        }
        SessionAction.getInstance().changeLanguage(request, type);
        return Optional.empty();
    }
}
