package com.marchenko.race.controller.command;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.domain.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/27/2015.
 */
public class FirstPageCommand implements Command {
    private static final String LOG_IN_URL = "/log_in";
    private static final String TABLE_RACES_URL = "/races";

    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        User.Role role = SessionAction.getInstance().takeRole(request);
        if (role == User.Role.LOG_OUT_USER) {
            return Optional.of(LOG_IN_URL);
        } else {
            return Optional.of(TABLE_RACES_URL);
        }
    }
}
