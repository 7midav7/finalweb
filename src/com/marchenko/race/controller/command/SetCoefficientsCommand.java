package com.marchenko.race.controller.command;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.controller.parameter.SettingCoefficientsParameter;
import com.marchenko.race.controller.validation.SettingCoefficientsValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.domain.Race;
import com.marchenko.race.service.CoefficientsService;
import com.marchenko.race.service.RaceService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/18/2015.
 */
public class SetCoefficientsCommand implements Command {
    private static final String TABLE_RACES_URL = "/races";

    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        SettingCoefficientsParameter parameter = new SettingCoefficientsParameter();
        parameter.initialize(request);
        Validator<SettingCoefficientsParameter> validator = SettingCoefficientsValidator.getInstance();
        Optional<String> optional = validator.receiveMistakeMessageIfExist(parameter);

        if (!optional.isPresent()){
            CoefficientsService coefficientService = CoefficientsService.getInstance();
            RaceService raceService = RaceService.getInstance();
            try {
                coefficientService.addHorseCoefficients(parameter);
                raceService.changeStage(parameter.getRaceId(), Race.Stage.WAITING_RESULTS);
                return Optional.of(TABLE_RACES_URL);
            } catch (ServiceException e) {
                throw new ControllerException(e);
            }
        } else {
            String message = optional.get();
            MessageAction.getInstance().transferErrorMessage(request, message);
        }
        return Optional.empty();
    }
}
