package com.marchenko.race.controller.command;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.controller.parameter.SettingResultsParameter;
import com.marchenko.race.controller.validation.SettingResultsValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.domain.Race;
import com.marchenko.race.domain.Result;
import com.marchenko.race.service.RaceService;
import com.marchenko.race.service.ResultService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class SetResultsCommand implements Command {
    private static final String TABLE_RACES_URL = "/races";

    @Override
    public Optional<String> execute(HttpServletRequest request) throws ControllerException {
        SettingResultsParameter parameter = new SettingResultsParameter();
        parameter.initialize(request);
        Validator<SettingResultsParameter> validator = SettingResultsValidator.getInstance();
        Optional<String> message = validator.receiveMistakeMessageIfExist(parameter);

        if (!message.isPresent()){
            List<Result> results = parameter.transferDomain();
            ResultService resultService = ResultService.getInstance();
            RaceService raceService = RaceService.getInstance();
            try {
                resultService.addAllResults(results, parameter.getRaceId());
                raceService.changeStage(parameter.getRaceId(), Race.Stage.FINAL_STAGE);
                return Optional.of(TABLE_RACES_URL);
            } catch (ServiceException e) {
                throw new ControllerException(e);
            }
        } else {
            String mes = message.get();
            MessageAction.getInstance().transferErrorMessage(request, mes);
        }

        return Optional.empty();
    }
}
