package com.marchenko.race.controller.command;

import com.marchenko.race.controller.exception.ControllerException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/9/2015.
 */
public interface Command {
    Optional<String> execute(HttpServletRequest request) throws ControllerException;
}
