package com.marchenko.race.controller.command;

import java.util.HashMap;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/29/2015.
 */
public class CommandBuilder {
    private HashMap<String, Command> commands = new HashMap<>();

    private CommandBuilder() {
        commands.put("REGISTRATION", new RegistrationCommand());
        commands.put("LOG_IN", new LogInCommand());
        commands.put("LOG_OUT", new LogOutCommand());
        commands.put("FIRST_PAGE", new FirstPageCommand());
        commands.put("CREATE_RACE", new CreationRaceCommand());
        commands.put("DELETE_RACE", new DeleteRaceCommand());
        commands.put("CHANGE_LANGUAGE", new ChangeLanguageCommand());
        commands.put("TRANSFER_MONEY", new TransferFromBankCommand());
        commands.put("SET_COEFFICIENTS", new SetCoefficientsCommand());
        commands.put("BET", new BetCommand());
        commands.put("SET_RESULTS", new SetResultsCommand());
    }

    public static CommandBuilder getInstance(){
        return new CommandBuilder();
    }

    public Optional<Command> build(String name){
        if (name==null){
            return Optional.empty();
        }
        Command c = commands.get(name.toUpperCase());
        return (c != null) ? Optional.of(c): Optional.empty();
    }
}
