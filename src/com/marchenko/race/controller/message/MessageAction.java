package com.marchenko.race.controller.message;

import com.marchenko.race.controller.SessionAction;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/15/2015.
 */
public class MessageAction {
    private static final String REQUEST_MESSAGE_ATTRIBUTE = "message";
    private static final String REQUEST_TYPE_MESSAGE_ATTRIBUTE = "type_message";
    private static final String ERROR_TITLE = "error";
    private static final String NOTIFICATION_TITLE = "notification";
    private static final String CRUSHED_OPERATION_MESSAGE = "crushed.operation";
    private static final String SUCCESSFUL_OPERATION_MESSAGE = "successful.operation";
    private static MessageAction instance = new MessageAction();

    private MessageAction(){

    }

    public static MessageAction getInstance(){
        return instance;
    }

    public void transferErrorMessage(HttpServletRequest request, String message){
        String transformedMessage = takeMessage(message, request);
        String transformedErrorTitle = takeMessage(ERROR_TITLE, request);
        request.setAttribute(REQUEST_MESSAGE_ATTRIBUTE, transformedMessage);
        request.setAttribute(REQUEST_TYPE_MESSAGE_ATTRIBUTE, transformedErrorTitle);
    }

    public void transferNotificationMessage(HttpServletRequest request, String message){
        String transformedMessage = takeMessage(message, request);
        String transformedNotificationTitle = takeMessage(NOTIFICATION_TITLE, request);
        request.setAttribute(REQUEST_MESSAGE_ATTRIBUTE, transformedMessage);
        request.setAttribute(REQUEST_TYPE_MESSAGE_ATTRIBUTE, transformedNotificationTitle);
    }

    public String takeMessage(String message, HttpServletRequest request){
        LocalizationManager.TypeLocalization typeLocale = SessionAction.getInstance().takeLanguage(request);
        return takeMessage(message, typeLocale);
    }

    public String takeMessage(String message, LocalizationManager.TypeLocalization typeLocalization){
        Localization localization = LocalizationManager.getInstance().getLocalization(typeLocalization);
        return localization.getString(message);
    }

    public void transferCrushedOperationErrorMessage(HttpServletRequest request){
        transferErrorMessage(request, CRUSHED_OPERATION_MESSAGE);
    }

    public void transferSuccessfulOperationMessage(HttpServletRequest request){
        transferNotificationMessage(request, SUCCESSFUL_OPERATION_MESSAGE);
    }
}
