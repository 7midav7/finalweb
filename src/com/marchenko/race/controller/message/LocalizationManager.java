package com.marchenko.race.controller.message;

import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Marchenko Vadim on 12/8/2015.
 */
public class LocalizationManager {
    public enum TypeLocalization{RU_RU, EN_US}
    private static final String BASENAME_ERROR_MESSAGES = "property.message";
    private static Lock lock = new ReentrantLock();
    private static LocalizationManager instance;
    private HashMap<TypeLocalization, Localization> map;

    private LocalizationManager(){
        map = new HashMap<>();
        map.put(TypeLocalization.RU_RU, new Localization(BASENAME_ERROR_MESSAGES, "RU", "RU"));
        map.put(TypeLocalization.EN_US, new Localization(BASENAME_ERROR_MESSAGES, "EN", "US"));
    }

    public static LocalizationManager getInstance(){
        if (instance == null){
            lock.lock();
            if (instance == null){
                instance = new LocalizationManager();
            }
            lock.unlock();
        }
        return instance;
    }

    public Localization getLocalization(TypeLocalization type){
        Localization localization = map.get(type);
        if (localization == null){
            localization = map.get(TypeLocalization.EN_US);
        }
        return localization;
    }
}
