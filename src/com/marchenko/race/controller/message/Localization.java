package com.marchenko.race.controller.message;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Marchenko Vadim on 12/8/2015.
 */
public class Localization {
    private ResourceBundle bundle;

    public Localization(String baseName, String language, String country) {
        Locale locale = new Locale(language, country);
        bundle = ResourceBundle.getBundle(baseName, locale);
    }

    public String getString(String name){
        return bundle.getString(name);
    }
}
