package com.marchenko.race.controller.page;

import com.marchenko.race.controller.exception.ControllerException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/12/2015.
 */
public interface Page {
    String preparePage(HttpServletRequest request) throws ControllerException;
}
