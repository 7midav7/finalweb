package com.marchenko.race.controller.page;

import com.marchenko.race.controller.exception.ControllerException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/21/2015.
 */
public class NotFoundPage implements Page {
    private static final String NOT_FOUND_PAGE = "/page/log_out_user/not_found.jsp";

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        return NOT_FOUND_PAGE;
    }
}
