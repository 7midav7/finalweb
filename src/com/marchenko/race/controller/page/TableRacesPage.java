package com.marchenko.race.controller.page;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.ViewRaceAttribute;
import com.marchenko.race.domain.Race;
import com.marchenko.race.domain.User;
import com.marchenko.race.service.RaceService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marchenko Vadim on 12/12/2015.
 */
public class TableRacesPage implements Page {
    private static final String TABLE_RACES_ADMIN = "/page/admin/table_races.jsp";
    private static final String TABLE_RACES_BOOKMAKER = "/page/bookmaker/table_races.jsp";
    private static final String TABLE_RACES_USER = "/page/user/table_races.jsp";
    private static final String TABLE_RACES_LOG_OUT_USER = "/page/log_out_user/table_races.jsp";
    private static final String VIEW_RACE_LIST_PARAMETER = "list";

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        RaceService service = RaceService.getInstance();
        try {
            List<Race> races =  service.takeAllRaces();
            races.sort((a, b) -> ((-1) * a.getTimeStarting().compareTo(b.getTimeStarting())));
            ArrayList<ViewRaceAttribute> listView = new ArrayList<>();
            for (Race race : races){
                listView.add(new ViewRaceAttribute(race));
            }
            request.setAttribute(VIEW_RACE_LIST_PARAMETER, listView);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
        return UrlDependingRole.getInstance().takeUrl(request,
                TABLE_RACES_ADMIN, TABLE_RACES_BOOKMAKER, TABLE_RACES_USER, TABLE_RACES_LOG_OUT_USER);
    }
}
