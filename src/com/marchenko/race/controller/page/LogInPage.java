package com.marchenko.race.controller.page;

import com.marchenko.race.controller.exception.ControllerException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/12/2015.
 */
public class LogInPage implements Page {
    private static final String LOG_IN_PAGE = "/page/log_out_user/login.jsp";

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        return LOG_IN_PAGE;
    }
}
