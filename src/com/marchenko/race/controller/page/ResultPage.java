package com.marchenko.race.controller.page;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.RaceIdParameter;
import com.marchenko.race.controller.parameter.ViewResultAttribute;
import com.marchenko.race.controller.parameter.ViewResultBetsAttribute;
import com.marchenko.race.controller.validation.RaceIdResultsValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.domain.*;
import com.marchenko.race.service.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class ResultPage implements Page {
    private static final String ADMIN_RESULTS_PAGE = "/page/admin/results.jsp";
    private static final String BOOKMAKER_RESULT_PAGE = "/page/bookmaker/results.jsp";
    private static final String LOG_OUT_USER_RESULT_PAGE = "/page/log_out_user/results.jsp";
    private static final String USER_RESULTS_PAGE = "/page/user/results.jsp";
    private static final String NOT_FOUND_PAGE = "/page/log_out_user/not_found.jsp";
    private static final String REQUEST_ATTRIBUTE_RACE_ID = "id";
    private static final String REQUEST_ATTRIBUTE_LIST = "list";
    private static final String REQUEST_ATTRIBUTE_AMOUNT_WIN = "amountWin";

    private class PairCoefficientBet {
        private Bet bet;
        private Coefficient coefficient;

        public PairCoefficientBet(Bet bet, Coefficient coefficient) {
            this.bet = bet;
            this.coefficient = coefficient;
        }

        public Bet getBet() {
            return bet;
        }

        public Coefficient getCoefficient() {
            return coefficient;
        }
    }

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        RaceIdParameter raceIdParameter = new RaceIdParameter();
        raceIdParameter.initialize(request);
        Validator<RaceIdParameter> validator = RaceIdResultsValidator.getInstance();
        boolean isValid = validator.isValid(raceIdParameter);

        if (!isValid) {
            return NOT_FOUND_PAGE;
        }

        User.Role role = SessionAction.getInstance().takeRole(request);
        if (role == User.Role.USER) {
            makeAttributeUser(request, raceIdParameter.getId());
        } else {
            makeAttribute(request, raceIdParameter.getId());
        }

        request.setAttribute(REQUEST_ATTRIBUTE_RACE_ID, raceIdParameter.getId());

        return UrlDependingRole.getInstance().takeUrl(request,
                ADMIN_RESULTS_PAGE, BOOKMAKER_RESULT_PAGE, USER_RESULTS_PAGE, LOG_OUT_USER_RESULT_PAGE);
    }

    private void makeAttribute(HttpServletRequest request, int raceId) throws ControllerException {
        ResultService resultService = ResultService.getInstance();
        RaceService raceService = RaceService.getInstance();
        try {
            List<Result> results = resultService.findByRaceId(raceId);
            List<Horse> horses = raceService.findHorses(raceId);
            results.sort((a, b) -> Integer.compare(a.getHorseId(), b.getHorseId()));
            horses.sort((a, b) -> Integer.compare(a.getId(), b.getId()));
            List<ViewResultAttribute> viewResults = new ArrayList<>();
            for (int i = 0; i < results.size(); ++i) {
                ViewResultAttribute viewResult =
                        new ViewResultAttribute(horses.get(i).getName(), results.get(i).getPosition());
                viewResults.add(viewResult);
            }
            viewResults.sort((a, b) -> Integer.compare(a.getPosition(), b.getPosition()));
            request.setAttribute(REQUEST_ATTRIBUTE_LIST, viewResults);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }

    private void makeAttributeUser(HttpServletRequest request, int raceId) throws ControllerException {
        ResultService resultService = ResultService.getInstance();
        RaceService raceService = RaceService.getInstance();
        CoefficientsService coefficientsService = CoefficientsService.getInstance();
        BetService betService = BetService.getInstance();

        Optional<Integer> userIdOptional = SessionAction.getInstance().takeUserId(request);
        int userId = userIdOptional.get();

        List<Result> results;
        List<Horse> horses;
        List<Coefficient> coefficients;
        List<Bet> bets;

        try {
            results = resultService.findByRaceId(raceId);
            horses = raceService.findHorses(raceId);
            coefficients = coefficientsService.findCoefficient(raceId);
            bets = betService.findBets(userId, raceId);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }

        coefficients.sort((a, b) -> Integer.compare(a.getId(), b.getId()));
        bets.sort((a, b) -> Integer.compare(a.getCoefficientId(), b.getCoefficientId()));
        List<PairCoefficientBet> pairs = new ArrayList<>();
        for (int i = 0; i < coefficients.size(); ++i) {
            PairCoefficientBet pair = new PairCoefficientBet(bets.get(i), coefficients.get(i));
            pairs.add(pair);
        }

        results.sort((a, b) -> Integer.compare(a.getHorseId(), b.getHorseId()));
        horses.sort((a, b) -> Integer.compare(a.getId(), b.getId()));
        pairs.sort((a, b) -> {
            int value = Integer.compare(a.getCoefficient().getHorseId(), b.getCoefficient().getHorseId());
            if (value != 0) {
                return value;
            } else {
                String place = Coefficient.Type.PLACE.toString();
                return (-1) * place.compareTo(a.getCoefficient().getType().toString());
            }
        });

        List<ViewResultBetsAttribute> viewResults = new ArrayList<>();
        for (int i = 0; i < results.size(); ++i) {
            ViewResultBetsAttribute attribute = new ViewResultBetsAttribute();
            Horse horse = horses.get(i);
            attribute.setHorseName(horse.getName());
            Result result = results.get(i);
            attribute.setPosition(result.getPosition());
            PairCoefficientBet pairWin = pairs.get(i * 2);
            attribute.setWinCoefficient(pairWin.getCoefficient().getCoefficient());
            attribute.setWinBet(pairWin.getBet().getAmount());
            PairCoefficientBet pairPlace = pairs.get(i * 2 + 1);
            attribute.setPlaceCoefficient(pairPlace.getCoefficient().getCoefficient());
            attribute.setPlaceBet(pairPlace.getBet().getAmount());
            viewResults.add(attribute);
        }

        viewResults.sort((a, b) -> Integer.compare(a.getPosition(), b.getPosition()));
        request.setAttribute(REQUEST_ATTRIBUTE_LIST, viewResults);

        double amountWin = viewResults.get(0).getWinCoefficient() * viewResults.get(0).getWinBet();
        amountWin += viewResults.get(0).getPlaceCoefficient() * viewResults.get(0).getPlaceBet();
        amountWin += viewResults.get(1).getPlaceCoefficient() * viewResults.get(1).getPlaceBet();
        amountWin += viewResults.get(2).getPlaceCoefficient() * viewResults.get(2).getPlaceBet();
        request.setAttribute(REQUEST_ATTRIBUTE_AMOUNT_WIN, amountWin);
    }
}
