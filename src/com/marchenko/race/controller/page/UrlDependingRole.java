package com.marchenko.race.controller.page;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.domain.User;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/21/2015.
 */
class UrlDependingRole {
    public static final String NOT_ENOUGH_RIGHTS = "/page/log_out_user/access_denied.jsp";
    private static final UrlDependingRole INSTANCE = new UrlDependingRole();

    private UrlDependingRole(){

    }

    public static UrlDependingRole getInstance(){
        return INSTANCE;
    }

    public String takeUrl(HttpServletRequest request, String adminUrl, String bookmakerUrl, String userUrl, String logOutUserUrl){
        User.Role role = SessionAction.getInstance().takeRole(request);
        switch (role){
            case ADMIN: return adminUrl;
            case BOOKMAKER: return bookmakerUrl;
            case USER: return userUrl;
            case LOG_OUT_USER: return logOutUserUrl;
            default:
                return NOT_ENOUGH_RIGHTS;
        }
    }
}
