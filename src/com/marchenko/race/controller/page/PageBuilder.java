package com.marchenko.race.controller.page;

import java.util.HashMap;

/**
 * Created by Marchenko Vadim on 12/12/2015.
 */
public class PageBuilder {
    private static final PageBuilder INSTANCE = new PageBuilder();
    private static final Page NOT_FOUND_PAGE = new NotFoundPage();
    private HashMap<String, Page> pages = new HashMap<>();

    private PageBuilder(){
        pages.put("RACES", new TableRacesPage());
        pages.put("CREATION_RACE", new CreationRacePage());
        pages.put("COEFFICIENTS", new CoefficientsPage());
        pages.put("REGISTRATION", new RegistrationPage());
        pages.put("LOG_IN", new LogInPage());
        pages.put("BALANCE", new BalancePage());
        pages.put("SET_COEFFICIENTS", new SettingCoefficientsPage());
        pages.put("SET_RESULTS", new SettingResultPage());
        pages.put("RESULTS", new ResultPage());
    }

    public static PageBuilder getInstance(){
        return INSTANCE;
    }

    public Page build(String name){
        if (name==null){
            return NOT_FOUND_PAGE;
        }
        Page page = pages.get(name.toUpperCase());
        return (page != null) ? page: NOT_FOUND_PAGE;
    }
}
