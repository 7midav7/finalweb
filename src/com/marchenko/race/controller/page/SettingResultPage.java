package com.marchenko.race.controller.page;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.RaceIdParameter;
import com.marchenko.race.controller.validation.RaceIdResultsValidator;
import com.marchenko.race.controller.validation.RaceIdSetResultsValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.domain.Horse;
import com.marchenko.race.service.RaceService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class SettingResultPage implements Page {
    private static final String URL_SET_RESULTS = "/page/admin/set_result.jsp";
    private static final String URL_PAGE_NOT_FOUND = "/page/log_out_user/not_found.jsp";
    private static final String REQUEST_ATTRIBUTE_ID = "id";
    private static final String REQUEST_ATTRIBUTE_LIST = "list";

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        RaceIdParameter raceIdParameter = new RaceIdParameter();
        raceIdParameter.initialize(request);
        Validator<RaceIdParameter> validator = RaceIdSetResultsValidator.getInstance();
        boolean isValid = validator.isValid(raceIdParameter);

        if (!isValid){
            return URL_PAGE_NOT_FOUND;
        }

        try {
            List<Horse> races = RaceService.getInstance().findHorses(raceIdParameter.getId());
            request.setAttribute(REQUEST_ATTRIBUTE_LIST, races);
            request.setAttribute(REQUEST_ATTRIBUTE_ID, raceIdParameter.getId());
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }

        return URL_SET_RESULTS;
    }
}
