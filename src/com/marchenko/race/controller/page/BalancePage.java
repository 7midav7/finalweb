package com.marchenko.race.controller.page;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.domain.RegisteredUser;
import com.marchenko.race.domain.User;
import com.marchenko.race.service.RegisteredUserService;
import com.marchenko.race.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/15/2015.
 */
public class BalancePage implements Page {
    private static final String URL_PROFILE_PAGE = "/page/user/balance.jsp";
    private static final String REQUEST_CASH_ATTRIBUTE = "cash";

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        Optional<Integer> idOptional = SessionAction.getInstance().takeUserId(request);
        int id = idOptional.get();
        try {
            RegisteredUserService service = RegisteredUserService.getInstance();
            Optional<RegisteredUser> optional = service.findUserById(id);
            if (optional.isPresent()){
                User user = optional.get();
                request.setAttribute(REQUEST_CASH_ATTRIBUTE, user.getCash());
            }
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
        return URL_PROFILE_PAGE;
    }
}
