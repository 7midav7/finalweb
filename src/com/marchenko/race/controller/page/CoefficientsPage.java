package com.marchenko.race.controller.page;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.CoefficientBetAttribute;
import com.marchenko.race.controller.parameter.RaceIdParameter;
import com.marchenko.race.controller.validation.RaceIdCoefficientValidator;
import com.marchenko.race.controller.validation.Validator;
import com.marchenko.race.domain.*;
import com.marchenko.race.service.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/12/2015.
 */
public class CoefficientsPage implements Page {
    private static final String ADMIN_RACE_DETAILS = "/page/admin/coefficients.jsp";
    private static final String BOOKMAKER_RACE_DETAILS = "/page/bookmaker/coefficients.jsp";
    private static final String USER_RACE_DETAILS = "/page/user/coefficients.jsp";
    private static final String LOG_OUT_RACE_DETAILS = "/page/log_out_user/coefficients.jsp";
    private static final String NOT_FOUND_PAGE = "/page/log_out_user/not_found.jsp";
    private static final String REQUEST_ATTRIBUTES_LIST_COEFFICIENTS = "list";
    private static final String REQUEST_ATTRIBUTES_LIST_COEFFICIENTS_BETS = "listBets";
    private static final String REQUEST_ATTRIBUTES_CASH = "cash";
    private static final String REQUEST_ATTRIBUTES_ID = "id";

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        RaceIdParameter parameter = new RaceIdParameter();
        parameter.initialize(request);
        Validator<RaceIdParameter> validator = RaceIdCoefficientValidator.getInstance();
        boolean isValid = validator.isValid(parameter);

        if (!isValid){
            return NOT_FOUND_PAGE;
        }

        User.Role role = SessionAction.getInstance().takeRole(request);

        if (role == User.Role.USER){
            makeAttributesUser(request, parameter);
        } else {
            makeAttribute(request, parameter);
        }

        return UrlDependingRole.getInstance().takeUrl(request, ADMIN_RACE_DETAILS,
                BOOKMAKER_RACE_DETAILS, USER_RACE_DETAILS, LOG_OUT_RACE_DETAILS);
    }

    public void makeAttributesUser(HttpServletRequest request, RaceIdParameter raceIdParameter) throws ControllerException {
        try {
            CoefficientsService coefficientsService = CoefficientsService.getInstance();
            BetService betService = BetService.getInstance();

            List<Coefficient> coefficients;
            List<Bet> bets;
            Optional<Integer> userIdOptional = SessionAction.getInstance().takeUserId(request);
            int userId = userIdOptional.get();

            try {
                coefficients = coefficientsService.findCoefficient(raceIdParameter.getId());
                bets = betService.findBets(userId, raceIdParameter.getId());
            } catch (ServiceException e) {
                throw new ControllerException(e);
            }

            coefficients.sort((a,b) -> (Integer.compare(a.getId(), b.getId())));
            bets.sort((a,b) -> (Integer.compare(a.getCoefficientId(), b.getCoefficientId())));
            List<CoefficientBetAttribute> pairs = new ArrayList<>();

            if (bets.isEmpty()){
                coefficients.sort(
                        (a, b) -> {
                            int aId = a.getHorseId();
                            int bId = b.getHorseId();
                            if (Integer.compare( aId , bId ) != 0){
                                return Integer.compare( aId , bId );
                            } else {
                                String place = Coefficient.Type.PLACE.toString();
                                return (-1)*place.compareTo(a.getType().toString());
                            }
                        }
                );
                request.setAttribute(REQUEST_ATTRIBUTES_LIST_COEFFICIENTS, coefficients);
            } else {
                for (int i = 0; i < coefficients.size(); ++ i){
                    CoefficientBetAttribute pairCoefficientBet =
                            new CoefficientBetAttribute(bets.get(i), coefficients.get(i));
                    pairs.add(pairCoefficientBet);
                }

                pairs.sort(
                        (a, b) -> {
                            Coefficient aCoefficient = a.getCoefficient();
                            Coefficient bCoefficient = b.getCoefficient();
                            int aId = aCoefficient.getHorseId();
                            int bId = bCoefficient.getId();
                            if (Integer.compare(aId, bId) != 0) {
                                return Integer.compare(aId, bId);
                            } else {
                                String place = Coefficient.Type.PLACE.toString();
                                return (-1) * place.compareTo(aCoefficient.getType().toString());
                            }
                        }
                );
                request.setAttribute(REQUEST_ATTRIBUTES_LIST_COEFFICIENTS_BETS, pairs);
            }

            request.setAttribute(REQUEST_ATTRIBUTES_ID, raceIdParameter.getId());
            Optional<Double> optional = RegisteredUserService.getInstance().findCash(userId);
            double cash = optional.get();
            request.setAttribute(REQUEST_ATTRIBUTES_CASH, cash);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }

    public void makeAttribute(HttpServletRequest request, RaceIdParameter parameter) throws ControllerException {
        try {
            CoefficientsService service = CoefficientsService.getInstance();
            List<Coefficient> coefficients = service.findCoefficient(parameter.getId());
            coefficients.sort(
                    (a, b) -> {
                        int aId = a.getHorseId();
                        int bId = b.getHorseId();
                        if (Integer.compare( aId , bId ) != 0){
                            return Integer.compare( aId , bId );
                        } else {
                            String place = Coefficient.Type.PLACE.toString();
                            return (-1)*place.compareTo(a.getType().toString());
                        }
                    }
            );
            request.setAttribute(REQUEST_ATTRIBUTES_LIST_COEFFICIENTS, coefficients);
            request.setAttribute(REQUEST_ATTRIBUTES_ID, parameter.getId());
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }
}
