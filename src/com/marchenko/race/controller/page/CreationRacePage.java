package com.marchenko.race.controller.page;

import com.marchenko.race.controller.exception.ControllerException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/12/2015.
 */
public class CreationRacePage implements Page {
    private static final String URL_CREATE_PAGE_PAGE = "/page/admin/creation_race.jsp";

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        return URL_CREATE_PAGE_PAGE;
    }
}
