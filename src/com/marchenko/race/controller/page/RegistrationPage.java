package com.marchenko.race.controller.page;

import com.marchenko.race.controller.exception.ControllerException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/12/2015.
 */
public class RegistrationPage implements Page {
    private static final String REGISTRATION_PAGE = "/page/log_out_user/registration.jsp";

    @Override
    public String preparePage(HttpServletRequest request) throws ControllerException {
        return REGISTRATION_PAGE;
    }
}
