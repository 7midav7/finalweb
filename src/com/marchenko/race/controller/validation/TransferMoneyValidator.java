package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.TransferMoneyParameter;
import com.marchenko.race.domain.BankAccount;
import com.marchenko.race.service.BankService;
import com.marchenko.race.service.ServiceException;

import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Created by Marchenko Vadim on 12/16/2015.
 */
// TODO: think about concurrency
public class TransferMoneyValidator implements Validator<TransferMoneyParameter> {
    private static final String ILLEGAL_FORMAT_AMOUNT = "illegal.format.amount.cash";
    private static final String ILLEGAL_FORMAT_ACCOUNT_NAME = "illegal.format.id.account";
    private static final String UNKNOWN_ACCOUNT_NAME = "unknown.account.name";
    private static final String NOT_ENOUGH_MONEY = "not.enough.money";
    private static final String REGEX_FORMAT_ACCOUNT_NAME = "\\d{5}";
    private static final TransferMoneyValidator INSTANCE = new TransferMoneyValidator();

    private TransferMoneyValidator(){

    }

    public static TransferMoneyValidator getInstance(){
        return INSTANCE;
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(TransferMoneyParameter parameter) throws ControllerException {
        Optional<String> optional = receiveInitializationMessage(parameter);
        if (optional.isPresent()){
            return optional;
        }
        double amount = parameter.getAmount();
        if (!validFormatAmount(amount)){
            return Optional.of(ILLEGAL_FORMAT_AMOUNT);
        }
        String name = parameter.getAccountName();
        if (!validFormatAccountName(name)){
            return Optional.of(ILLEGAL_FORMAT_ACCOUNT_NAME);
        }
        if (!isExistingAccountName(name)){
            return Optional.of(UNKNOWN_ACCOUNT_NAME);
        }
        if (!isMoneyEnough(name, amount)){
            return Optional.of(NOT_ENOUGH_MONEY);
        }
        return Optional.empty();
    }

    private boolean validFormatAmount(double amount){
        return amount > 0;
    }

    private boolean validFormatAccountName(String name){
        if (name == null){
            return false;
        }
        return Pattern.matches(REGEX_FORMAT_ACCOUNT_NAME, name);
    }

    private boolean isExistingAccountName(String name) throws ControllerException {
        BankService service = BankService.getInstance();
        try {
            return service.isValidAccount(name);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }

    private boolean isMoneyEnough(String name, double amount) throws ControllerException {
        BankService service = BankService.getInstance();
        try{
            return service.isEnoughCash(name, amount);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }
}
