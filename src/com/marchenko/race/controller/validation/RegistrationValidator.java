package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.RegistrationParameter;
import com.marchenko.race.service.RegisteredUserService;
import com.marchenko.race.service.ServiceException;

import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Created by Marchenko Vadim on 11/19/2015.
 */

public class RegistrationValidator implements Validator<RegistrationParameter> {
    private static final String PATTERN_USERNAME = "([\\w]){1,12}";
    private static final String PATTERN_PASSWORD = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[\\w]{8,20}$";
    private static final String PATTERN_PHONE = "\\+?\\(?\\d{2,4}\\)?[\\d\\s-]{3,}";
    private static final String PATTERN_EMAIL = "[\\w\\.-]+@[a-zA-Z\\d-]+\\.[a-zA-Z-\\d\\.]+";
    private static final String ILLEGAL_FORMAT_USERNAME = "illegal.format.username";
    private static final String ILLEGAL_FORMAT_PASSWORD = "illegal.format.password";
    private static final String PASSWORDS_ARE_DIFFERENT = "password.different";
    private static final String ILLEGAL_FORMAT_EMAIL = "illegal.format.email";
    private static final String ILLEGAL_FORMAT_PHONE = "illegal.format.phone";
    private static final String USERNAME_NOT_UNIQUE = "username.not.unique";
    private static RegistrationValidator instance = new RegistrationValidator();

    private RegistrationValidator() {
    }

    public static RegistrationValidator getInstance() {
        return instance;
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(RegistrationParameter parameter) throws ControllerException{
        Optional<String> optional = receiveInitializationMessage(parameter);
        if (optional.isPresent()){
            return optional;
        }
        String message = null;
        if (!isValidUsername(parameter.getUsername())){
            message = ILLEGAL_FORMAT_USERNAME;
        }
        if (!isValidPassword(parameter.getPassword())){
            message = ILLEGAL_FORMAT_PASSWORD;
        }
        if (!isValidPasswords(parameter.getPassword(), parameter.getDuplicatePassword())){
            message = PASSWORDS_ARE_DIFFERENT;
        }
        if (!isValidEmail(parameter.getEmail())){
            message = ILLEGAL_FORMAT_EMAIL;
        }
        if (!isValidPhone(parameter.getPhone())){
            message = ILLEGAL_FORMAT_PHONE;
        }
        if (!isUsernameUnique(parameter.getUsername())){
            message = USERNAME_NOT_UNIQUE;
        }
        if (message == null){
            return Optional.empty();
        } else {
            return Optional.of(message);
        }
    }

    private boolean isValidUsername(String username) {
        if (username == null) return false;
        return Pattern.matches(PATTERN_USERNAME, username);
    }

    private boolean isValidPasswords(String password, String duplicatePassword) {
        if (password == null || duplicatePassword == null) return false;
        return password.equals(duplicatePassword);
    }

    private boolean isValidPassword(String password){
        if (password == null) return false;
        return Pattern.matches(PATTERN_PASSWORD, password);
    }

    private boolean isValidEmail(String email) {
        if (email == null) return false;
        return Pattern.matches(PATTERN_EMAIL, email);
    }

    private boolean isValidPhone(String phone) {
        if (phone == null) return false;
        return Pattern.matches(PATTERN_PHONE, phone);
    }

    private boolean isUsernameUnique(String username) throws ControllerException{
        RegisteredUserService service = RegisteredUserService.getInstance();
        try {
            return !service.isDatabaseContainUsername(username);
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }
}
