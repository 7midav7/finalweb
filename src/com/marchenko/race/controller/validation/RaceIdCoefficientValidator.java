package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.RaceIdParameter;
import com.marchenko.race.domain.Coefficient;
import com.marchenko.race.service.CoefficientsService;
import com.marchenko.race.service.ServiceException;

import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/21/2015.
 */
public class RaceIdCoefficientValidator implements Validator<RaceIdParameter> {
    private static final RaceIdCoefficientValidator INSTANCE = new RaceIdCoefficientValidator();

    private RaceIdCoefficientValidator(){

    }

    public static RaceIdCoefficientValidator getInstance(){
        return INSTANCE;
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(RaceIdParameter parameter) throws ControllerException {
        return Optional.empty();
    }

    @Override
    public boolean isValid(RaceIdParameter parameter) throws ControllerException {
        Optional<String> optional = receiveInitializationMessage(parameter);
        if (optional.isPresent()){
            return false;
        }
        CoefficientsService service = CoefficientsService.getInstance();
        try {
            List<Coefficient> coefficients = service.findCoefficient(parameter.getId());
            return !coefficients.isEmpty();
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }
}
