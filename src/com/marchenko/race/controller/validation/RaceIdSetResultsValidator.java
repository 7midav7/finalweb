package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.RaceIdParameter;
import com.marchenko.race.domain.Race;
import com.marchenko.race.service.RaceService;
import com.marchenko.race.service.ServiceException;

import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/21/2015.
 */
public class RaceIdSetResultsValidator implements Validator<RaceIdParameter> {
    private static final RaceIdSetResultsValidator INSTANCE = new RaceIdSetResultsValidator();

    private RaceIdSetResultsValidator(){

    }

    public static RaceIdSetResultsValidator getInstance(){
        return INSTANCE;
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(RaceIdParameter parameter) throws ControllerException {
        return Optional.empty();
    }

    @Override
    public boolean isValid(RaceIdParameter parameter) throws ControllerException {
        Optional<String> optional = receiveInitializationMessage(parameter);
        if (optional.isPresent()){
            return false;
        }
        RaceService service = RaceService.getInstance();
        try {
            Optional<Race.Stage> stageOptional = service.takeCurrentStage(parameter.getId());
            if (!stageOptional.isPresent()){
                return false;
            } else {
                Race.Stage stage = stageOptional.get();
                return stage == Race.Stage.WAITING_RESULTS;
            }
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }
}
