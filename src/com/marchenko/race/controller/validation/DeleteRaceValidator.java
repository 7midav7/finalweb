package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.parameter.DeleteRaceParameter;

import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/4/2015.
 */
public class DeleteRaceValidator implements Validator<DeleteRaceParameter>{
    private static DeleteRaceValidator instance = new DeleteRaceValidator();

    private DeleteRaceValidator(){

    }

    public static DeleteRaceValidator getInstance() {
        return instance;
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(DeleteRaceParameter parameter) {
        Optional<String> optional = receiveInitializationMessage(parameter);
        if (optional.isPresent()){
            return optional;
        }
        return Optional.empty();
    }
}
