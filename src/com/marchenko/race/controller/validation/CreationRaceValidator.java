package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.CreationRaceParameter;
import com.marchenko.race.controller.parameter.RegistrationParameter;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/30/2015.
 */
public class CreationRaceValidator implements Validator<CreationRaceParameter> {
    private static final String PATTERN_NAME_RACE = ".{1,25}";
    private static final String PATTERN_HORSE_NAME = ".{1,25}";
    private static final String MESSAGE_INVALID_TIME_FORMAT = "invalid.time.format";
    private static CreationRaceValidator instance = new CreationRaceValidator();

    private CreationRaceValidator(){

    }

    public static CreationRaceValidator getInstance() {
        return instance;
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(CreationRaceParameter parameter) throws ControllerException {
        Optional<String> optional = receiveInitializationMessage(parameter);
        if (optional.isPresent()){
            return optional;
        }
        if (!isValidTimeStarting(parameter.getTimeStarting())){
            return Optional.of(MESSAGE_INVALID_TIME_FORMAT);
        }
        return Optional.empty();
    }

    private boolean isValidCount(){
        return true;
    }

    private boolean isValidNameRace(){
        return true;
    }

    private boolean isValidTimeStarting(LocalDateTime timeParameter){
        LocalDateTime time = LocalDateTime.now();
        return time.compareTo(timeParameter) < 0;
    }

    private boolean isValidHorseName(){
        return true;
    }
}
