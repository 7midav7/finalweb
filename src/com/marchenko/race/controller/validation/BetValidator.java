package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.BetParameter;
import com.marchenko.race.service.RegisteredUserService;
import com.marchenko.race.service.ServiceException;

import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class BetValidator implements Validator<BetParameter> {
    public static final BetValidator INSTANCE = new BetValidator();
    public static final String NOT_ENOUGH_MONEY = "not.enough.money";

    private BetValidator(){

    }

    public static BetValidator getInstance(){
        return INSTANCE;
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(BetParameter parameter) throws ControllerException {
        Optional<String> initializeMessage = receiveInitializationMessage(parameter);
        if (initializeMessage.isPresent()){
            return initializeMessage;
        }
        if (!isEnoughMoney(parameter)){
            return Optional.of(NOT_ENOUGH_MONEY);
        }
        return Optional.empty();
    }

    private boolean isEnoughMoney(BetParameter parameter) throws ControllerException {
        int userId = parameter.getUserId();
        try {
            Optional<Double> optionalCash = RegisteredUserService.getInstance().findCash(userId);
            double cash = optionalCash.get();
            int amountBet = 0;
            for (int i=0; i<parameter.getCount(); ++i){
                int bet = parameter.takeBet(i);
                if (bet > 0){
                    amountBet += bet;
                }
            }
            return amountBet <= cash;
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }
}
