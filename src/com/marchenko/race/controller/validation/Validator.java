package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.ParameterRequest;

import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/29/2015.
 */
public interface Validator<T extends ParameterRequest> {
    Optional<String> receiveMistakeMessageIfExist(T parameter) throws ControllerException;

    default boolean isValid(T parameter) throws ControllerException{
        return !receiveMistakeMessageIfExist(parameter).isPresent();
    }

    default Optional<String> receiveInitializationMessage(T parameter){
        final String INITIALIZATION_ERROR = "initialization.error";
        if (parameter.isInitialize()){
            return Optional.empty();
        } else {
            return Optional.of(INITIALIZATION_ERROR);
        }
    }
}
