package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.RaceIdParameter;
import com.marchenko.race.domain.Result;
import com.marchenko.race.service.ResultService;
import com.marchenko.race.service.ServiceException;


import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/21/2015.
 */
public class RaceIdResultsValidator implements Validator<RaceIdParameter> {
    private static final RaceIdResultsValidator INSTANCE = new RaceIdResultsValidator();

    private RaceIdResultsValidator(){
    }

    public static RaceIdResultsValidator getInstance(){
        return INSTANCE;
    }

    @Override
    public boolean isValid(RaceIdParameter parameter) throws ControllerException {
        Optional<String> optional = receiveInitializationMessage(parameter);
        if (optional.isPresent()){
            return false;
        }
        ResultService service = ResultService.getInstance();
        try {
            List<Result> results = service.findByRaceId(parameter.getId());
            return !results.isEmpty();
        } catch (ServiceException e) {
            throw new ControllerException(e);
        }
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(RaceIdParameter parameter) throws ControllerException {
        return Optional.empty();
    }


}
