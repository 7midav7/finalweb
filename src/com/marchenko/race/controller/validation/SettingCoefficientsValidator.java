package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.SettingCoefficientsParameter;

import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/18/2015.
 */
public class SettingCoefficientsValidator implements Validator<SettingCoefficientsParameter> {
    private static final SettingCoefficientsValidator INSTANCE = new SettingCoefficientsValidator();

    private SettingCoefficientsValidator(){

    }

    public static SettingCoefficientsValidator getInstance(){
        return INSTANCE;
    }


    @Override
    public Optional<String> receiveMistakeMessageIfExist(SettingCoefficientsParameter parameter) throws ControllerException {
        Optional<String> optional = receiveInitializationMessage(parameter);
        if (optional.isPresent()){
            return optional;
        }
        return Optional.empty();
    }
}
