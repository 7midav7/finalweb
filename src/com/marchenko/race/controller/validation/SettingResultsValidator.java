package com.marchenko.race.controller.validation;

import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.parameter.SettingCoefficientsParameter;
import com.marchenko.race.controller.parameter.SettingResultsParameter;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class SettingResultsValidator implements Validator<SettingResultsParameter> {
    private static final SettingResultsValidator INSTANCE = new SettingResultsValidator();
    private static final String MESSAGE_INVALID_POSITION_FORMAT = "invalid.position.format";

    private SettingResultsValidator(){

    }

    public static SettingResultsValidator getInstance(){
        return INSTANCE;
    }

    @Override
    public Optional<String> receiveMistakeMessageIfExist(SettingResultsParameter parameter) throws ControllerException {
        Optional<String> optionalInitialization = receiveInitializationMessage(parameter);
        if (optionalInitialization.isPresent()){
            return optionalInitialization;
        }
        ArrayList<Integer> positions = new ArrayList<>();
        for (int i=0; i<parameter.getCount(); ++ i){
            positions.add(parameter.takePositions(i));
        }
        if (!isAllPositions(positions)){
            return Optional.of(MESSAGE_INVALID_POSITION_FORMAT);
        }
        return Optional.empty();
    }

    private boolean isAllPositions(ArrayList<Integer> positions){
        positions.sort(Integer::compare);
        int n = positions.size();
        if (positions.get(0) != 1 || positions.get(n-1) != n){
            return false;
        }
        for (int i=0; i < positions.size() - 1; ++ i){
            if (positions.get(i).equals(positions.get(i + 1))){
                return false;
            }
        }
        return true;
    }
}
