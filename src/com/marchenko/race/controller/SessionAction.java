package com.marchenko.race.controller;

import com.marchenko.race.controller.message.LocalizationManager;
import com.marchenko.race.domain.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/15/2015.
 */
public class SessionAction {
    private static final SessionAction INSTANCE = new SessionAction();
    private static final String SESSION_ATTRIBUTE_ID = "id";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";
    private static final String SESSION_ATTRIBUTE_USERNAME = "username";
    private static final String SESSION_ATTRIBUTE_LANGUAGE = "language";

    private SessionAction(){

    }

    public static SessionAction getInstance(){
        return INSTANCE;
    }

    public void logInUser(HttpServletRequest request, User user){
        HttpSession session = request.getSession();
        session.setAttribute(SESSION_ATTRIBUTE_ID, user.getId());
        session.setAttribute(SESSION_ATTRIBUTE_ROLE, user.getRole());
        session.setAttribute(SESSION_ATTRIBUTE_USERNAME, user.getUsername());
    }

    public void logOutUser(HttpServletRequest request){
        HttpSession session = request.getSession();
        session.removeAttribute(SESSION_ATTRIBUTE_ROLE);
        session.removeAttribute(SESSION_ATTRIBUTE_USERNAME);
    }

    public void changeLanguage(HttpServletRequest request, LocalizationManager.TypeLocalization localization){
        HttpSession session = request.getSession();
        session.setAttribute(SESSION_ATTRIBUTE_LANGUAGE, localization);
    }

    public LocalizationManager.TypeLocalization takeLanguage(HttpServletRequest request){
        HttpSession session = request.getSession();
        LocalizationManager.TypeLocalization type =
                (LocalizationManager.TypeLocalization)session.getAttribute(SESSION_ATTRIBUTE_LANGUAGE);
        if (type == null){
            type = LocalizationManager.TypeLocalization.EN_US;
        }
        return type;
    }

    public User.Role takeRole(HttpServletRequest request){
        HttpSession session = request.getSession();
        User.Role role = (User.Role) session.getAttribute(SESSION_ATTRIBUTE_ROLE);
        if (role != null){
            return role;
        } else {
            return User.Role.LOG_OUT_USER;
        }
    }

    public Optional<String> takeUsername(HttpServletRequest request){
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute(SESSION_ATTRIBUTE_USERNAME);
        if (username != null){
            return Optional.of(username);
        } else {
            return Optional.empty();
        }
    }

    public Optional<Integer> takeUserId(HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer id = (Integer) session.getAttribute(SESSION_ATTRIBUTE_ID);
        if (id != null){
            return Optional.of(id);
        } else {
            return Optional.empty();
        }
    }

    public String takeUrlJsp(HttpServletRequest request){
        User.Role role = takeRole(request);
        return "/page/" + role.toString().toLowerCase();
    }
}
