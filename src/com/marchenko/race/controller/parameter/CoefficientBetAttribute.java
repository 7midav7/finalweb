package com.marchenko.race.controller.parameter;

import com.marchenko.race.domain.Bet;
import com.marchenko.race.domain.Coefficient;

/**
 * Created by Marchenko Vadim on 12/22/2015.
 */
public class CoefficientBetAttribute {
    private Bet bet;
    private Coefficient coefficient;

    public CoefficientBetAttribute(Bet bet, Coefficient coefficient) {
        this.bet = bet;
        this.coefficient = coefficient;
    }

    public Bet getBet() {
        return bet;
    }

    public Coefficient getCoefficient() {
        return coefficient;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }

    public void setCoefficient(Coefficient coefficient) {
        this.coefficient = coefficient;
    }
}
