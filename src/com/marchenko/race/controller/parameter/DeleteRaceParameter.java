package com.marchenko.race.controller.parameter;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/4/2015.
 */
public class DeleteRaceParameter extends ParameterRequest {
    private static final String PARAM_ID = "id";
    private int id;

    @Override
    public void initialize(HttpServletRequest request) {
        try {
            String strId = request.getParameter(PARAM_ID);
            if (strId != null) {
                id = Integer.parseInt(strId);
                super.initialize(request);
            }
        } catch (NumberFormatException e) {
            return;
        }
    }

    public int getId() {
        return id;
    }
}
