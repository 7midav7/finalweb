package com.marchenko.race.controller.parameter;


import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.message.MessageAction;
import com.marchenko.race.controller.message.LocalizationManager;
import com.marchenko.race.domain.Race;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Marchenko Vadim on 12/1/2015.
 */

public class ViewRaceAttribute {
    private static final Logger LOG = LogManager.getLogger();
    private static final String MESSAGE_WAITING_COEFFICIENTS = "waiting.coefficients.stage";
    private static final String MESSAGE_WAITING_RESULTS = "waiting.results.stage";
    private static final String MESSAGE_FINAL_STAGE = "final.stage";
    private int id;
    private String nameRace;
    private String dateStarting;
    private int countHorses;
    private Race.Stage stage;

    public ViewRaceAttribute(Race race){
        id = race.getId();
        nameRace = race.getNameRace();
        countHorses = race.getCountHorses();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime time = race.getTimeStarting();
        dateStarting = time.format(formatter);
        stage = race.getStage();
    }

    public int getId() {
        return id;
    }

    public String getNameRace() {
        return nameRace;
    }

    public String getDateStarting() {
        return dateStarting;
    }

    public int getCountHorses() {
        return countHorses;
    }

    public Race.Stage getStage() {
        return stage;
    }

    public void setNameRace(String nameRace) {
        this.nameRace = nameRace;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDateStarting(String dateStarting) {
        this.dateStarting = dateStarting;
    }

    public void setCountHorses(int countHorses) {
        this.countHorses = countHorses;
    }

    public void setStage(Race.Stage stage) {
        this.stage = stage;
    }
}
