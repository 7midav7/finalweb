package com.marchenko.race.controller.parameter;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/4/2015.
 */
public abstract class ParameterRequest {
    private boolean isInitialize;

    public void initialize(HttpServletRequest request){
        isInitialize = true;
    }

    public boolean isInitialize() {
        return isInitialize;
    }
}
