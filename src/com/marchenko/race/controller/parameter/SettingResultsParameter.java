package com.marchenko.race.controller.parameter;

import com.marchenko.race.domain.Result;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class SettingResultsParameter extends ParameterRequest {
    private static final String PARAM_COUNT = "count";
    private static final String PARAM_RACE_ID = "id";
    private static final String PARAM_HORSE_ID = "horse-id-";
    private static final String PARAM_POSITION = "position-";
    private int count;
    private int raceId;
    private int[] horseIds;
    private int[] positions;

    @Override
    public void initialize(HttpServletRequest request) {
        try{
            String strCount = request.getParameter(PARAM_COUNT);
            count = Integer.parseInt(strCount);
            String strRaceId = request.getParameter(PARAM_RACE_ID);
            raceId = Integer.parseInt(strRaceId);
            horseIds = new int[count];
            positions = new int[count];
            for (int i = 0; i < count; ++i) {
                String strHorseId = request.getParameter(PARAM_HORSE_ID + i);
                horseIds[i] = Integer.parseInt(strHorseId);
                String strPosition = request.getParameter(PARAM_POSITION + i);
                positions[i] = Integer.parseInt(strPosition);
            }
        } catch (NullPointerException| NumberFormatException e){
            return;
        }
        super.initialize(request);
    }

    public List<Result> transferDomain(){
        List<Result> results = new ArrayList<>();
        for (int i = 0; i < count; ++i){
            Result result = new Result();
            result.setRaceId(raceId);
            result.setHorseId(horseIds[i]);
            result.setPosition(positions[i]);
            results.add(result);
        }
        return results;
    }

    public int getCount() {
        return count;
    }

    public int getRaceId() {
        return raceId;
    }

    public int takeHorseId(int index){
        return horseIds[index];
    }

    public int takePositions(int index){
        return positions[index];
    }
}
