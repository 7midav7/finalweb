package com.marchenko.race.controller.parameter;


import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

/**
 * Created by Marchenko Vadim on 11/30/2015.
 */
public class CreationRaceParameter extends ParameterRequest{
    private static final String PARAM_NAME_RACE = "name_race";
    private static final String PARAM_COUNT_HORSES = "count_horses";
    private static final String PARAM_TIME_STARTING = "time_starting";
    private static final String PARAM_NAME_HORSE = "horse_name";
    private String nameRace;
    private LocalDateTime timeStarting;
    private int countHorses;
    private String[] names;

    @Override
    public void initialize(HttpServletRequest request){
        try {
            nameRace = request.getParameter(PARAM_NAME_RACE);
            timeStarting = LocalDateTime.parse(request.getParameter(PARAM_TIME_STARTING));
            countHorses = Integer.parseInt(request.getParameter(PARAM_COUNT_HORSES));
            names = new String[countHorses];
            for (int i = 1; i <= countHorses; ++i) {
                names[i - 1] = request.getParameter(PARAM_NAME_HORSE + i);
            }
        } catch (NumberFormatException | DateTimeParseException | NullPointerException e){
            return;
        }
        super.initialize(request);
    }

    public String getNameRace() {
        return nameRace;
    }

    public LocalDateTime getTimeStarting() {
        return timeStarting;
    }

    public int getCountHorses() {
        return countHorses;
    }

    public String[] getNames() {
        return names.clone();
    }
}
