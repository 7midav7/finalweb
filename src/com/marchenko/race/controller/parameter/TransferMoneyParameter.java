package com.marchenko.race.controller.parameter;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/16/2015.
 */
public class TransferMoneyParameter extends ParameterRequest {
    private static final String PARAM_AMOUNT = "amount";
    private static final String PARAM_ACCOUNT_ID = "account_id";
    private double amount;
    private String accountId;

    @Override
    public void initialize(HttpServletRequest request) {
        try{
            accountId = request.getParameter(PARAM_ACCOUNT_ID);
            amount = Double.parseDouble(request.getParameter(PARAM_AMOUNT));
        } catch (NumberFormatException | NullPointerException e){
            return;
        }
        super.initialize(request);
    }

    public double getAmount() {
        return amount;
    }

    public String getAccountName() {
        return accountId;
    }
}
