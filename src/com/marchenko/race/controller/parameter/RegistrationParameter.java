package com.marchenko.race.controller.parameter;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 11/30/2015.
 */
public class RegistrationParameter extends ParameterRequest {
    private static final String PARAM_NAME_USERNAME = "username";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_DUPLICATE_PASSWORD = "duplicate-password";
    private static final String PARAM_NAME_PHONE = "phone";
    private static final String PARAM_NAME_EMAIL = "email";
    private String username;
    private String password;
    private String duplicatePassword;
    private String phone;
    private String email;

    @Override
    public void initialize(HttpServletRequest req) {
        username = req.getParameter(PARAM_NAME_USERNAME);
        password = req.getParameter(PARAM_NAME_PASSWORD);
        duplicatePassword = req.getParameter(PARAM_NAME_DUPLICATE_PASSWORD);
        phone = req.getParameter(PARAM_NAME_PHONE);
        email = req.getParameter(PARAM_NAME_EMAIL);
        super.initialize(req);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDuplicatePassword() {
        return duplicatePassword;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }
}
