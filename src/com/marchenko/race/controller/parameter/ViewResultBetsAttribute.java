package com.marchenko.race.controller.parameter;

/**
 * Created by Marchenko Vadim on 12/21/2015.
 */
public class ViewResultBetsAttribute {
    private String horseName;
    private int position;
    private double winCoefficient;
    private double placeCoefficient;
    private int winBet;
    private int placeBet;

    public String getHorseName() {
        return horseName;
    }

    public void setHorseName(String horseName) {
        this.horseName = horseName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public double getWinCoefficient() {
        return winCoefficient;
    }

    public void setWinCoefficient(double winCoefficient) {
        this.winCoefficient = winCoefficient;
    }

    public double getPlaceCoefficient() {
        return placeCoefficient;
    }

    public void setPlaceCoefficient(double placeCoefficient) {
        this.placeCoefficient = placeCoefficient;
    }

    public int getWinBet() {
        return winBet;
    }

    public void setWinBet(int betWin) {
        this.winBet = betWin;
    }

    public int getPlaceBet() {
        return placeBet;
    }

    public void setPlaceBet(int betPlace) {
        this.placeBet = betPlace;
    }
}
