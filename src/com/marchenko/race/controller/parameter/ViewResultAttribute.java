package com.marchenko.race.controller.parameter;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class ViewResultAttribute {
    private String horseName;
    private int position;

    public ViewResultAttribute(String horseName, int position) {
        this.horseName = horseName;
        this.position = position;
    }

    public String getHorseName() {
        return horseName;
    }

    public void setHorseName(String horseName) {
        this.horseName = horseName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
