package com.marchenko.race.controller.parameter;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

/**
 * Created by Marchenko Vadim on 12/18/2015.
 */
public class SettingCoefficientsParameter extends ParameterRequest {
    private static final String PARAM_COUNT = "count";
    private static final String PARAM_ID = "id";
    private static final String PARAM_WIN_COEFFICIENT = "win-coefficient-";
    private static final String PARAM_PLACE_COEFFICIENT = "place-coefficient-";
    private static final String PARAM_HORSE_ID = "horse-id-";
    private static final String REGEX_COEFFICIENTS = "([1-9]|(1[0-9]))";
    private int raceId;
    private int count;
    private double[] winCoefficients;
    private double[] placeCoefficients;
    private int[] horseId;

    @Override
    public void initialize(HttpServletRequest request) {
        try{
            String strCount = request.getParameter(PARAM_COUNT);
            count = Integer.parseInt(strCount);
            String strId = request.getParameter(PARAM_ID);
            raceId = Integer.parseInt(strId);
            winCoefficients = new double[count];
            placeCoefficients = new double[count];
            horseId = new int[count];
            for (int i = 0; i < count; ++ i){
                String strCoefWin = request.getParameter(PARAM_WIN_COEFFICIENT+i);
                String strCoefPlace = request.getParameter(PARAM_PLACE_COEFFICIENT+i);
                boolean winValid = Pattern.matches(REGEX_COEFFICIENTS, strCoefWin);
                boolean placeValid = Pattern.matches(REGEX_COEFFICIENTS, strCoefPlace);
                if ((!winValid) || (!placeValid)){
                    return;
                }
                String strHorseId = request.getParameter(PARAM_HORSE_ID+i);
                winCoefficients[i] = Double.parseDouble(strCoefWin);
                placeCoefficients[i] = Double.parseDouble(strCoefPlace);
                horseId[i] = Integer.parseInt(strHorseId);
            }
        } catch (NullPointerException | NumberFormatException e){
            return;
        }
        super.initialize(request);
    }

    public int getRaceId() {
        return raceId;
    }

    public int getCount() {
        return count;
    }

    public int takeHorseId(int index){
        return horseId[index];
    }

    public double takeWinCoefficient(int index){
        return winCoefficients[index];
    }

    public double takePlaceCoefficient(int index){
        return placeCoefficients[index];
    }
}
