package com.marchenko.race.controller.parameter;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.domain.Bet;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class BetParameter extends ParameterRequest {
    private static final String PARAM_COUNT = "count";
    private static final String PARAM_AMOUNT_BET = "bet-";
    private static final String PARAM_COEFFICIENT = "id-coefficient-";
    private int count;
    private int[] coefficientId;
    private int[] amountBet;
    private int userId;

    @Override
    public void initialize(HttpServletRequest request) {
        String strCount = request.getParameter(PARAM_COUNT);
        Optional<Integer> userIdOptional = SessionAction.getInstance().takeUserId(request);
        userId = userIdOptional.get();
        try{
            count = Integer.parseInt(strCount);
            coefficientId = new int[count];
            amountBet = new int[count];
            for (int i = 0; i < count; ++ i){
                String strAmount = request.getParameter(PARAM_AMOUNT_BET+i);
                amountBet[i] = Integer.parseInt(strAmount);
                String strCoef = request.getParameter(PARAM_COEFFICIENT+i);
                coefficientId[i] = Integer.parseInt(strCoef);
            }
        } catch (NumberFormatException | NullPointerException e){
            return;
        }
        super.initialize(request);
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCount() {
        return count;
    }

    public int takeBet(int index){
        return amountBet[index];
    }

    public int takeCoefficientId(int index){
        return coefficientId[index];
    }

    public List<Bet> transferDomain(){
        ArrayList<Bet> list = new ArrayList<>();
        for (int i = 0; i < count; ++ i){
            Bet bet = new Bet();
            bet.setAmount(amountBet[i]);
            bet.setCoefficientId(coefficientId[i]);
            bet.setUserId(userId);
            list.add(bet);
        }
        return list;
    }
}
