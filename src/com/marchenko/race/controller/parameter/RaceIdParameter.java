package com.marchenko.race.controller.parameter;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Marchenko Vadim on 12/21/2015.
 */
public class RaceIdParameter extends ParameterRequest {
    private static final String RACE_ID_PARAMETER = "id";
    private int id;

    @Override
    public void initialize(HttpServletRequest request) {
        String strId = request.getParameter(RACE_ID_PARAMETER);
        if (strId == null){
            return;
        }
        try{
            id = Integer.parseInt(strId);
        } catch (NumberFormatException e){
            return;
        }
        super.initialize(request);
    }

    public int getId() {
        return id;
    }
}
