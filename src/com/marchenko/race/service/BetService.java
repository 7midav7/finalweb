package com.marchenko.race.service;

import com.marchenko.race.dao.BetDao;
import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.domain.Bet;

import java.util.List;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class BetService {
    private static final BetService INSTANCE = new BetService();

    private BetService(){

    }

    public static BetService getInstance(){
        return INSTANCE;
    }

    public void makeBets(List<Bet> bets, int userId) throws ServiceException {
        BetDao dao = BetDao.getInstance();
        try {
            dao.insertAll(bets, userId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Bet> findBets(int userId, int raceId) throws ServiceException {
        BetDao dao = BetDao.getInstance();
        try{
            return dao.findByUserIdRaceId(userId, raceId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
