package com.marchenko.race.service;

import com.marchenko.race.controller.parameter.RegistrationParameter;
import com.marchenko.race.dao.RegisteredUserDao;
import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.domain.RegisteredUser;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Optional;


/**
 * Created by Marchenko Vadim on 11/23/2015.
 */
public class RegisteredUserService {
    private static RegisteredUserService instance = new RegisteredUserService();

    private RegisteredUserService(){

    }

    public static RegisteredUserService getInstance(){
        return instance;
    }

    public boolean isDatabaseContainUsername(String username) throws ServiceException{
        RegisteredUserDao dao = RegisteredUserDao.getInstance();
        try {
            Optional<RegisteredUser> userOptional = dao.findByUsername(username);
            return userOptional.isPresent();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void registration(RegistrationParameter parameters) throws ServiceException{
        RegisteredUser user = new RegisteredUser();
        user.setUsername(parameters.getUsername());
        user.setPassword(DigestUtils.sha256Hex(parameters.getPassword()));
        user.setEmail(parameters.getEmail());
        user.setPhone(parameters.getPhone());
        RegisteredUserDao dao = RegisteredUserDao.getInstance();
        try {
            dao.insert(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<RegisteredUser> findUserDatabaseByUsernamePassword(String username, String password)
            throws ServiceException {
        RegisteredUserDao dao = RegisteredUserDao.getInstance();
        try {
            Optional<RegisteredUser> userOptional = dao.findByUsername(username);
            if (userOptional.isPresent()) {
                RegisteredUser user = userOptional.get();
                if (!user.getPassword().equals(DigestUtils.sha256Hex(password))) {
                    userOptional = Optional.empty();
                }
            }
            return userOptional;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<RegisteredUser> findUserByUsername(String username) throws ServiceException{
        try {
             return RegisteredUserDao.getInstance().findByUsername(username);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<Double> findCash(int id) throws ServiceException {
        try {
            RegisteredUserDao dao = RegisteredUserDao.getInstance();
            Optional<RegisteredUser> userOptional = dao.findById(id);
            if (userOptional.isPresent()){
                RegisteredUser user = userOptional.get();
                return Optional.of(user.getCash());
            } else {
                return Optional.empty();
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<RegisteredUser> findUserById(int id) throws ServiceException {
        try{
            RegisteredUserDao dao = RegisteredUserDao.getInstance();
            return dao.findById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
