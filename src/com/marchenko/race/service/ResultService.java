package com.marchenko.race.service;

import com.marchenko.race.dao.ResultDao;
import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.domain.Result;

import java.util.List;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class ResultService {
    private static final ResultService INSTANCE = new ResultService();

    private ResultService(){

    }

    public static ResultService getInstance(){
        return INSTANCE;
    }

    public void addAllResults(List<Result> results, int raceId) throws ServiceException {
        ResultDao dao = ResultDao.getInstance();
        try {
            dao.insertAll(results, raceId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Result> findByRaceId(int raceId) throws ServiceException {
        ResultDao dao = ResultDao.getInstance();
        try{
            return dao.findByRaceId(raceId);
        } catch (DaoException e){
            throw new ServiceException(e);
        }
    }
}
