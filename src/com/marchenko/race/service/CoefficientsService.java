package com.marchenko.race.service;

import com.marchenko.race.controller.parameter.SettingCoefficientsParameter;
import com.marchenko.race.dao.CoefficientDao;
import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.domain.Coefficient;

import java.util.List;

/**
 * Created by Marchenko Vadim on 12/18/2015.
 */
public class CoefficientsService {
    private static final CoefficientsService INSTANCE = new CoefficientsService();

    private CoefficientsService(){

    }

    public static CoefficientsService getInstance(){
        return INSTANCE;
    }

    public void addHorseCoefficients(SettingCoefficientsParameter parameters) throws ServiceException {
        CoefficientDao dao = CoefficientDao.getInstance();

        for (int i = 0; i < parameters.getCount(); ++ i) {
            Coefficient coefficientWin = new Coefficient();
            coefficientWin.setRaceId(parameters.getRaceId());
            coefficientWin.setCoefficient(parameters.takeWinCoefficient(i));
            coefficientWin.setType(Coefficient.Type.WIN);
            coefficientWin.setHorseId(parameters.takeHorseId(i));
            Coefficient coefficientPlace = new Coefficient();
            coefficientPlace.setRaceId(parameters.getRaceId());
            coefficientPlace.setCoefficient(parameters.takePlaceCoefficient(i));
            coefficientPlace.setType(Coefficient.Type.PLACE);
            coefficientPlace.setHorseId(parameters.takeHorseId(i));
            try {
                dao.insert(coefficientWin);
                dao.insert(coefficientPlace);
            } catch (DaoException e) {
                throw new ServiceException(e);
            }
        }
    }

    public List<Coefficient> findCoefficient(int raceId) throws ServiceException {
        try {
            return CoefficientDao.getInstance().findByRaceId(raceId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
