package com.marchenko.race.service;

import com.marchenko.race.dao.BankAccountDao;
import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.domain.BankAccount;

import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/16/2015.
 */
public class BankService {
    private static final BankService INSTANCE = new BankService();

    private BankService(){

    }

    public static BankService getInstance(){
        return INSTANCE;
    }

    public boolean isValidAccount(String name) throws ServiceException {
        BankAccountDao dao = BankAccountDao.getInstance();
        Optional<BankAccount> optional;
        try {
            optional = dao.findByName(name);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return optional.isPresent();
    }

    public boolean isEnoughCash(String name, double cash) throws ServiceException {
        BankAccountDao dao = BankAccountDao.getInstance();
        Optional<BankAccount> optional;
        try {
            optional = dao.findByName(name);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        if (optional.isPresent()){
            BankAccount account = optional.get();
            return account.getCash() > cash;
        } else {
            return false;
        }
    }

    public boolean transferBankUser(String nameAccount, double cash, int userId) throws ServiceException {
        BankAccountDao dao = BankAccountDao.getInstance();
        try {
            return dao.transferMoneyBankUser(nameAccount, cash, userId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public boolean transferUserBank(String name, int cash, int userId){
        return false;
    }
}
