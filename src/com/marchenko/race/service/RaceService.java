package com.marchenko.race.service;

import com.marchenko.race.controller.parameter.CreationRaceParameter;
import com.marchenko.race.dao.HorseDao;
import com.marchenko.race.dao.RaceDao;
import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.domain.Horse;
import com.marchenko.race.domain.Race;

import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/30/2015.
 */
public class RaceService {
    private static RaceService instance = new RaceService();

    private RaceService(){

    }

    public static RaceService getInstance(){
        return instance;
    }

    public void addRace(CreationRaceParameter parameters) throws ServiceException {
        Race race = new Race();
        race.setNameRace(parameters.getNameRace());
        race.setCountHorses(parameters.getCountHorses());
        race.setTimeStarting(parameters.getTimeStarting());
        RaceDao raceDao = RaceDao.getInstance();
        try {
            int id = raceDao.insert(race);
            int countHorses = parameters.getCountHorses();
            String[] names = parameters.getNames();
            for (int i = 0; i < countHorses; ++ i){
                Horse horse = new Horse();
                horse.setRaceId(id);
                horse.setName(names[i]);
                HorseDao horseDao = HorseDao.getInstance();
                horseDao.insert(horse);
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Race> takeAllRaces() throws ServiceException {
        RaceDao dao = RaceDao.getInstance();
        try {
            return dao.find();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void deleteRaceById(int raceId) throws ServiceException {
        RaceDao raceDao = RaceDao.getInstance();
        HorseDao horseDao = HorseDao.getInstance();
        try {
            raceDao.delete(raceId);
            List<Horse> list = horseDao.findByRaceId(raceId);
            for (Horse horse: list){
                horseDao.deleteById(horse.getId());
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Horse> findHorses(int raceId) throws ServiceException{
        HorseDao dao = HorseDao.getInstance();
        try {
            List<Horse> list = dao.findByRaceId(raceId);
            return list;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void changeStage(int raceId, Race.Stage stage) throws ServiceException {
        RaceDao dao = RaceDao.getInstance();
        try{
            dao.changeStatus(raceId, stage);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public boolean doesRaceExist(int raceId) throws ServiceException {
        RaceDao dao = RaceDao.getInstance();
        try{
            Optional<Race> optional = dao.findRaceById(raceId);
            return optional.isPresent();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<Race.Stage> takeCurrentStage(int raceId) throws ServiceException{
        RaceDao dao = RaceDao.getInstance();
        try{
            Optional<Race> optional = dao.findRaceById(raceId);
            if (optional.isPresent()){
                Race race = optional.get();
                return Optional.of(race.getStage());
            } else {
                return Optional.empty();
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
