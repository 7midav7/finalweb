package com.marchenko.race.dao;

import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.dao.pool.ConnectionPool;
import com.marchenko.race.domain.Horse;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marchenko Vadim on 12/1/2015.
 */
public class HorseDao {
    private static final String SQL_INSERT = "INSERT INTO horses(name, race_id) values(?, ?)";
    private static final String SQL_FIND_BY_RACE_ID =
            "SELECT id, name FROM horses WHERE race_id = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM horses WHERE id=?";
    private static HorseDao instance = new HorseDao();

    private HorseDao(){

    }

    public static HorseDao getInstance(){
        return instance;
    }

    public int insert(Horse horse) throws DaoException{
        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection();
            PreparedStatement statement =
                    connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, horse.getName());
            statement.setInt(2, horse.getRaceId());
            statement.executeUpdate();
            ResultSet generateKeys = statement.getGeneratedKeys();
            generateKeys.next();
            return generateKeys.getInt(1);
        }
        catch (SQLException e){
            throw new DaoException(e);
        }
    }

    public List<Horse> findByRaceId(int raceId) throws DaoException {
        List<Horse> list = new ArrayList<>();

        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_RACE_ID)){
            statement.setInt(1, raceId);
            try(ResultSet set = statement.executeQuery()){
                while (set.next()){
                    int id = set.getInt(1);
                    String name = set.getString(2);

                    Horse horse = new Horse();
                    horse.setId(id);
                    horse.setName(name);
                    horse.setRaceId(raceId);
                    list.add(horse);
                }
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return list;
    }

    public void deleteById(int id) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)){
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
