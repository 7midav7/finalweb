package com.marchenko.race.dao;

import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.dao.pool.ConnectionPool;
import com.marchenko.race.domain.Bet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class BetDao {
    public static final String SQL_INSERT = "INSERT INTO bets (coefficient_id, user_id, amount) VALUES (?, ?, ?)";
    public static final String SQL_REMOVE_AMOUNT = "UPDATE users SET cash=cash-? WHERE id=?";
    public static final String SQL_FIND_BY_RACE_ID_USER_ID =
            "SELECT bets.id, bets.coefficient_id, bets.amount FROM bets "
                    + "INNER JOIN coefficients ON coefficients.id=bets.coefficient_id "
                    + "WHERE coefficients.race_id = ? AND bets.user_id = ?";
    public static final BetDao INSTANCE = new BetDao();

    private BetDao() {
    }

    public static BetDao getInstance(){
        return INSTANCE;
    }

    public void insertAll(List<Bet> bets, int userId) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try(Connection connection = connectionPool.takeConnection()){
            int summaryAmount = 0;
            connection.setAutoCommit(false);
            for (Bet bet : bets){
                summaryAmount += bet.getAmount();
                try(PreparedStatement statement = connection.prepareStatement(SQL_INSERT)){
                    statement.setInt(1, bet.getCoefficientId());
                    statement.setInt(2, bet.getUserId());
                    statement.setInt(3, bet.getAmount());
                    statement.executeUpdate();
                }
            }
            try(PreparedStatement statement = connection.prepareStatement(SQL_REMOVE_AMOUNT)){
                statement.setInt(1, summaryAmount);
                statement.setInt(2, userId);
                statement.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    public List<Bet> findByUserIdRaceId(int userId, int raceId) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_RACE_ID_USER_ID)){
            statement.setInt(1, raceId);
            statement.setInt(2, userId);
            List<Bet> bets = new ArrayList<>();
            try(ResultSet set = statement.executeQuery()){
                while (set.next()){
                    Bet bet = new Bet();
                    bet.setUserId(userId);
                    bet.setId(set.getInt(1));
                    bet.setCoefficientId(set.getInt(2));
                    bet.setAmount(set.getInt(3));
                    bets.add(bet);
                }
            }
            return bets;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
