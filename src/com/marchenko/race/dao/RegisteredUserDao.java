package com.marchenko.race.dao;

import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.dao.pool.ConnectionPool;
import com.marchenko.race.domain.RegisteredUser;
import com.marchenko.race.domain.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/9/2015.
 */
public class RegisteredUserDao {
    private static final String SQL_INSERT_USER =
            "INSERT INTO Users(username, password, cash, email, phone, role) values( ?, ?, 0, ?, ?, 'user')";
    private static final String SQL_FIND_USER_USERNAME =
            "SELECT id, password, cash, email, phone, role FROM users WHERE username=?";
    private static final String SQL_FIND_USER_ID =
            "SELECT username, password, cash, email, phone, role FROM users WHERE id=?";
    private static final RegisteredUserDao INSTANCE = new RegisteredUserDao();

    private RegisteredUserDao(){

    }

    public static RegisteredUserDao getInstance(){
        return INSTANCE;
    }


    public void insert(RegisteredUser user) throws DaoException{
        ConnectionPool pool = ConnectionPool.getInstance();

        try(Connection connection = pool.takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER)) {
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPhone());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    //TODO: refactor on LIST
    public Optional<RegisteredUser> findByUsername(String username) throws DaoException{
        ConnectionPool pool = ConnectionPool.getInstance();
        Optional<RegisteredUser> optionalUser = Optional.empty();

        try(Connection connection = pool.takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_USER_USERNAME)) {
            statement.setString(1, username);
            try(ResultSet set = statement.executeQuery()){
                if ( set.next() ) {
                    RegisteredUser user = new RegisteredUser();
                    int id = set.getInt(1);
                    String password = set.getString(2);
                    double cash = set.getDouble(3);
                    String email = set.getString(4);
                    String phone = set.getString(5);
                    RegisteredUser.Role role = User.Role.valueOf(set.getString(6).toUpperCase());

                    user.setId(id);
                    user.setUsername(username);
                    user.setCash(cash);
                    user.setPassword(password);
                    user.setEmail(email);
                    user.setPhone(phone);
                    user.setRole(role);

                    optionalUser = Optional.of(user);
                }
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return optionalUser;
    }

    public Optional<RegisteredUser> findById(int id) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try(Connection connection = connectionPool.takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_USER_ID)) {
            statement.setInt(1, id);
            try(ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    RegisteredUser user = new RegisteredUser();
                    String username = set.getString(1);
                    String password = set.getString(2);
                    double cash = set.getDouble(3);
                    String email = set.getString(4);
                    String phone = set.getString(5);
                    String strRole = set.getString(6).toUpperCase();
                    RegisteredUser.Role role = User.Role.valueOf(strRole);

                    user.setId(id);
                    user.setUsername(username);
                    user.setCash(cash);
                    user.setPassword(password);
                    user.setEmail(email);
                    user.setPhone(phone);
                    user.setRole(role);

                    return Optional.of(user);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
