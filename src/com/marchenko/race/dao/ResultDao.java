package com.marchenko.race.dao;

import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.dao.pool.ConnectionPool;
import com.marchenko.race.domain.Result;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marchenko Vadim on 12/20/2015.
 */
public class ResultDao {
    private static final ResultDao INSTANCE = new ResultDao();
    private static final String SQL_INSERT_RESULT = "INSERT INTO results (race_id, horse_id, position) VALUES(?, ?, ?)";
    private static final String SQL_FIND_BY_RACE_ID = "SELECT id, horse_id, position FROM results WHERE race_id = ?";

    private ResultDao(){

    }

    public static ResultDao getInstance(){
        return INSTANCE;
    }

    public void insertAll(List<Result> results, int raceId) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection()){
            connection.setAutoCommit(false);
            for (Result result: results){
                try(PreparedStatement statement = connection.prepareStatement(SQL_INSERT_RESULT)){
                    statement.setInt(1, result.getRaceId());
                    statement.setInt(2, result.getHorseId());
                    statement.setInt(3, result.getPosition());
                    statement.executeUpdate();
                }
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    public List<Result> findByRaceId(int raceId) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_RACE_ID)){
            statement.setInt(1, raceId);
            ArrayList<Result> results = new ArrayList<>();
            try(ResultSet set = statement.executeQuery()){
                while (set.next()){
                    Result result = new Result();
                    result.setId(set.getInt(1));
                    result.setHorseId(set.getInt(2));
                    result.setRaceId(raceId);
                    result.setPosition(set.getInt(3));
                    results.add(result);
                }
            }
            return results;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
