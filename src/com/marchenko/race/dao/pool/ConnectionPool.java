package com.marchenko.race.dao.pool;

/*import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger; */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    private static final int NUMBER_CONNECTIONS = 20;
    private static final String URL_DATABASE = "jdbc:mysql://localhost:3306/race";
    private static final String USER = "root";
    private static final String PASSWORD = "pass";
    private static ConnectionPool instance;
    private static AtomicBoolean isInitialize = new AtomicBoolean();
    private static Lock lock = new ReentrantLock();
    private ArrayBlockingQueue<ProxyConnection> connections;

    private ConnectionPool(){
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            LOGGER.fatal("Driver not found in " + ConnectionPool.class.getName(), e);
            throw new RuntimeException("Driver not found in " + ConnectionPool.class.getName());
        }
        connections = new ArrayBlockingQueue<>(NUMBER_CONNECTIONS);
        for (int i = 0; i < NUMBER_CONNECTIONS; ++ i){
            try {
                Connection connection = DriverManager.getConnection(URL_DATABASE, USER, PASSWORD);
                ProxyConnection proxy = new ProxyConnection(connection);
                connections.add(proxy);
            } catch (SQLException e) {
                LOGGER.error("Problems with creating connections in ConnectionPool");
            }
        }
    }

    public static ConnectionPool getInstance(){
        if (!isInitialize.get()){
            lock.lock();
            if (instance == null){
                instance = new ConnectionPool();
                isInitialize.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    public Connection takeConnection(){
        ProxyConnection connection = null;
        try {
            connection = connections.take();
        } catch (InterruptedException e) {
            LOGGER.error("Pool connections crushed at connection method", e);
        }
        return connection;
    }

    public void close(){
        while (!connections.isEmpty()){
            try {
                Connection connection = connections.take();
                connection.close();
            } catch (SQLException e) {
                LOGGER.error("ConnectionPool can't close connection", e);
            } catch (InterruptedException e) {
                LOGGER.error("Something wrong with connections", e);
            }
        }
    }

    void putConnection(ProxyConnection connection){
        try {
            connections.put(connection);
        } catch (InterruptedException e) {
            LOGGER.error("Something wrong with connections", e);
        }
    }
}
