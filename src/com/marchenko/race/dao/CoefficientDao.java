package com.marchenko.race.dao;

import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.dao.pool.ConnectionPool;
import com.marchenko.race.domain.Coefficient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marchenko Vadim on 12/18/2015.
 */
public class CoefficientDao {
    private static final CoefficientDao INSTANCE = new CoefficientDao();
    private static final String SQL_INSERT =
            "INSERT INTO coefficients (horse_id, race_id, coefficient, type) VALUES (?, ?, ?, ?)";
    private static final String SQL_SELECT_ALL_COEFFICIENTS_BY_RACE_ID =
            "SELECT coefficients.id, coefficients.horse_id, coefficients.coefficient, coefficients.type, horses.name FROM coefficients \n" +
                    "INNER JOIN horses ON coefficients.horse_id=horses.id\n" +
                    "WHERE coefficients.race_id = ?";

    private CoefficientDao(){

    }

    public static CoefficientDao getInstance(){
        return INSTANCE;
    }

    public void insert(Coefficient element) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try(Connection connection = connectionPool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_INSERT)){
            statement.setInt(1, element.getHorseId());
            statement.setInt(2, element.getRaceId());
            statement.setDouble(3, element.getCoefficient());
            statement.setString(4, element.getType().toString());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    public List<Coefficient> findByRaceId(int raceId) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try(Connection connection = connectionPool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_COEFFICIENTS_BY_RACE_ID)){
            statement.setInt(1, raceId);
            ArrayList<Coefficient> list = new ArrayList<>();
            try(ResultSet set = statement.executeQuery()){
                while (set.next()){
                    int id = set.getInt(1);
                    int horseId = set.getInt(2);
                    double coefficient = set.getDouble(3);
                    String strType = set.getString(4);
                    Coefficient.Type type = Coefficient.Type.valueOf(strType);
                    String horseName = set.getString(5);
                    Coefficient horseCoefficient = new Coefficient();
                    horseCoefficient.setId(id);
                    horseCoefficient.setHorseId(horseId);
                    horseCoefficient.setCoefficient(coefficient);
                    horseCoefficient.setRaceId(raceId);
                    horseCoefficient.setType(type);
                    horseCoefficient.setHorseName(horseName);
                    list.add(horseCoefficient);
                }
            }
            return list;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
