package com.marchenko.race.dao;

import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.dao.pool.ConnectionPool;
import com.marchenko.race.domain.BankAccount;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 12/16/2015.
 */
public class BankAccountDao {
    private static final String SQL_FIND_BY_NAME = "SELECT id, cash FROM bank WHERE name=?";
    private static final String SQL_ADD_CASH_USER = "UPDATE users SET cash = cash + ? WHERE id=?";
    private static final String SQL_REMOVE_CASH_ACCOUNT = "UPDATE bank SET cash = cash - ? WHERE name=?";
    private static final BankAccountDao INSTANCE = new BankAccountDao();

    private BankAccountDao(){

    }

    public static BankAccountDao getInstance(){
        return INSTANCE;
    }


    public Optional<BankAccount> findByName(String name) throws DaoException {
        Optional<BankAccount> optional = Optional.empty();

        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try(Connection connection = connectionPool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME)){
            statement.setString(1, name);
            try(ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    int id = set.getInt(1);
                    int cash = set.getInt(2);
                    BankAccount account = new BankAccount();
                    account.setName(name);
                    account.setId(id);
                    account.setCash(cash);
                    optional = Optional.of(account);
                }
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        return optional;
    }

    public boolean transferMoneyBankUser(String nameAccount, double cash, int userId) throws DaoException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try(Connection connection = connectionPool.takeConnection()){
            connection.setAutoCommit(false);
            try(PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME)){
                statement.setString(1, nameAccount);
                try(ResultSet set = statement.executeQuery()){
                    set.next();
                    int cashAccount = set.getInt(2);
                    if (cashAccount < cash){
                        return false;
                    }
                }
            }
            try(PreparedStatement statement = connection.prepareStatement(SQL_ADD_CASH_USER)){
                statement.setDouble(1, cash);
                statement.setInt(2, userId);
                statement.executeUpdate();
            }
            try(PreparedStatement statement = connection.prepareStatement(SQL_REMOVE_CASH_ACCOUNT)){
                statement.setDouble(1, cash);
                statement.setString(2, nameAccount);
                statement.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return true;
    }
}
