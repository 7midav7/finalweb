package com.marchenko.race.dao;

import com.marchenko.race.dao.exception.DaoException;
import com.marchenko.race.dao.pool.ConnectionPool;
import com.marchenko.race.domain.Race;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/30/2015.
 */


public class RaceDao{
    private static final String SQL_INSERT_RACE =
            "INSERT INTO races(name, count_horses, date_starting, stage) values(?, ?, ?, ?)";
    private static final String SQL_FIND_ALL_RACES =
            "SELECT id, name, count_horses, date_starting, stage FROM races";
    private static final String SQL_FIND_RACE_BY_ID =
            "SELECT name, count_horses, date_starting, stage FROM races WHERE id=?";
    private static final String SQL_DELETE_RACE =
            "DELETE FROM races WHERE id=?";
    private static final String SQL_UPDATE_STAGE =
            "UPDATE races SET stage=? WHERE id=?";
    private static final RaceDao INSTANCE = new RaceDao();

    private RaceDao(){

    }

    public static RaceDao getInstance(){
        return INSTANCE;
    }

    public int insert(Race race) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        int id = 0;

        try(Connection connection = pool.takeConnection();
            PreparedStatement statement =
                    connection.prepareStatement(SQL_INSERT_RACE, Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, race.getNameRace());
            statement.setInt(2, race.getCountHorses());
            LocalDateTime time = race.getTimeStarting();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            String stringTime = time.format(formatter);
            statement.setString(3, stringTime);
            statement.setString(4, Race.Stage.WAITING_COEFFICIENTS.toString());
            statement.executeUpdate();
            ResultSet generatedKey = statement.getGeneratedKeys();
            if (generatedKey.next()){
                id = generatedKey.getInt(1);
            }
        } catch (SQLException e){
            throw new DaoException(e);
        }
        return id;
    }

    public List<Race> find() throws DaoException{
        ArrayList<Race> list = new ArrayList<>();
        ConnectionPool pool = ConnectionPool.getInstance();

        try(Connection connection = pool.takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_RACES);
            ResultSet resultSet = statement.executeQuery()){
            while (resultSet.next()){
                Race race = new Race();
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                int countHorses = resultSet.getInt(3);
                String dateStarting = resultSet.getString(4);
                Race.Stage stage = Race.Stage.valueOf(resultSet.getString(5));
                dateStarting = dateStarting.substring(0, dateStarting.length() - 2);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime time = LocalDateTime.parse(dateStarting, formatter);
                race.setId(id);
                race.setNameRace(name);
                race.setCountHorses(countHorses);
                race.setTimeStarting(time);
                race.setStage(stage);
                list.add(race);
            }
        } catch (SQLException e){
            throw new DaoException(e);
        }
        return list;
    }

    public void delete(int id) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_RACE)){
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    public void changeStatus(int id, Race.Stage stage) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_STAGE)){
            statement.setString(1, stage.toString());
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    public Optional<Race> findRaceById(int raceId) throws DaoException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try(Connection connection = pool.takeConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_FIND_RACE_BY_ID)){
            statement.setInt(1, raceId);
            try(ResultSet set = statement.executeQuery()){
                if (set.next()){
                    Race race = new Race();
                    String name = set.getString(1);
                    int countHorses = set.getInt(2);
                    String dateStarting = set.getString(3);
                    Race.Stage stage = Race.Stage.valueOf(set.getString(4));
                    dateStarting = dateStarting.substring(0, dateStarting.length() - 2);
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime time = LocalDateTime.parse(dateStarting, formatter);
                    race.setId(raceId);
                    race.setNameRace(name);
                    race.setCountHorses(countHorses);
                    race.setTimeStarting(time);
                    race.setStage(stage);

                    return Optional.of(race);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
