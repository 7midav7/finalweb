package com.marchenko.race.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Marchenko Vadim on 12/12/2015.
 */

@WebFilter(filterName = "UrlRewriteFilter", urlPatterns = {"/*"})
public class UrlRewriteFilter implements Filter {
    private static final String SITE = "http://localhost:8088";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        StringBuffer startUrl = request.getRequestURL();
        String endUrl = request.getQueryString();
        if (endUrl != null) {
            startUrl.append("?").append(endUrl);
        }
        String url = retainParameters(startUrl.toString());
        if (needToChange(url)) {
            String newUrl = transformNewUri(url);
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(newUrl);
            dispatcher.forward(request, response);
        } else {
            filterChain.doFilter(request, response);
        }
    }

    private String retainParameters(String url){
        return url.replace(SITE,"");
    }

    private boolean needToChange(String uri){
        if (uri == null || uri.isEmpty()) {
            return false;
        }
        return ! ( uri.contains("ServletController") || uri.contains(".") || uri.equals("/") );
    }

    @Override
    public void destroy() {

    }

    private String transformNewUri(String uri){
        String[] parameters = uri.split("\\?");
        ArrayList<String> list = new ArrayList<>();
        for (String str : parameters){
            if (!str.isEmpty()){
                list.add(str);
            }
        }
        StringBuilder builder = new StringBuilder("/ServletController");
        if (list.size()>0){
            String withoutSlash = list.get(0).substring(1);
            builder.append("?").append("page=").append(withoutSlash);
        }
        if (list.size()>1){
            String withoutQuestion = list.get(1).substring(1);
            builder.append("&").append(withoutQuestion);
        }
        return builder.toString();
    }
}
