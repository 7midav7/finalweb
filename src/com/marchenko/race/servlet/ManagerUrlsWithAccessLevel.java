package com.marchenko.race.servlet;

import com.marchenko.race.controller.SessionAction;
import com.marchenko.race.domain.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by Marchenko Vadim on 12/22/2015.
 */
public class ManagerUrlsWithAccessLevel {
    private class AccessFlags{
        private boolean admin;
        private boolean bookmaker;
        private boolean user;
        private boolean logOutUser;

        private AccessFlags(boolean admin, boolean bookmaker, boolean user, boolean logOutUser) {
            this.admin = admin;
            this.bookmaker = bookmaker;
            this.user = user;
            this.logOutUser = logOutUser;
        }
    }

    private static final ManagerUrlsWithAccessLevel INSTANCE = new ManagerUrlsWithAccessLevel();
    private HashMap<String, AccessFlags> map = new HashMap<>();


    private ManagerUrlsWithAccessLevel(){
        map.put("/page/admin/creation_race.jsp", new AccessFlags(true, false, false, false));
        map.put("/page/admin/creation_race.jsp", new AccessFlags(true, false, false, false));
        map.put("/page/admin/results.jsp", new AccessFlags(true, false, false, false));
        map.put("/page/admin/set_result.jsp", new AccessFlags(true, false, false, false));
        map.put("/page/admin/table_races.jsp", new AccessFlags(true, false, false, false));
        map.put("/page/bookmaker/coefficients.jsp", new AccessFlags(false, true, false, false));
        map.put("/page/bookmaker/results.jsp", new AccessFlags(false, true, false, false));
        map.put("/page/bookmaker/set_coefficient.jsp", new AccessFlags(false, true, false, false));
        map.put("/page/bookmaker/table_races.jsp", new AccessFlags(false, true, false, false));
        map.put("/page/user/balance.jsp", new AccessFlags(false, false, true, false));
        map.put("/page/user/coefficient.jsp", new AccessFlags(false, false, true, false));
        map.put("/page/user/results.jsp", new AccessFlags(false, false, true, false));
        map.put("/page/user/table_races.jsp", new AccessFlags(false, false, true, false));
        map.put("/balance", new AccessFlags(false, false, true, false));
        map.put("/creation_race", new AccessFlags(true, false, false, false));
        map.put("/set_coefficients", new AccessFlags(false, true, false, false));
        map.put("/set_results", new AccessFlags(true, false, false, false));
        map.put("create_race", new AccessFlags(true, false, false, false));
        map.put("create_race", new AccessFlags(true, false, false, false));
        map.put("delete_race", new AccessFlags(true, false, false, false));
        map.put("transfer_money", new AccessFlags(false, false, true, false));
        map.put("set_coefficients", new AccessFlags(false, true, false, false));
        map.put("bet", new AccessFlags(false, false, true, false));
        map.put("set_results", new AccessFlags(true, false, false, false));
    }

    public static ManagerUrlsWithAccessLevel getInstance(){
        return INSTANCE;
    }

    public boolean haveAccessRights(HttpServletRequest request, String url){
        AccessFlags flags = map.get(url.toLowerCase());
        if (flags == null){
            return true;
        }
        User.Role role = SessionAction.getInstance().takeRole(request);
        switch (role){
            case ADMIN: return flags.admin;
            case BOOKMAKER: return flags.bookmaker;
            case USER: return flags.user;
            case LOG_OUT_USER: return flags.logOutUser;
            default: return false;
        }
    }
}
