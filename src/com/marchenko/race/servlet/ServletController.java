package com.marchenko.race.servlet;

import com.marchenko.race.controller.command.Command;
import com.marchenko.race.controller.command.CommandBuilder;
import com.marchenko.race.controller.exception.ControllerException;
import com.marchenko.race.controller.page.Page;
import com.marchenko.race.controller.page.PageBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by Marchenko Vadim on 11/9/2015.
 */
//TODO: COEFFICIENTS WITH 12.12
//TODO: RIGHT DELETING WITH BETS!!!
// TODO: solve problem with connections!!!!!!!!!!!!!!! Sometimes throws NULL !!!!
// TODO: think about all data types id db
    //TODO: stars near input fields
//TODO: pages in html or jsp
    //TODO: refactor string literals in DAO with final static
    //TODO: rename files (create_race on creation)
@WebServlet(name="ServletController", urlPatterns = {"/ServletController"})
public class ServletController extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ServletController.class);
    private static final String ERROR_PAGE = "/page/log_out_user/error_page.jsp";

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        LOGGER.debug("command=" + request.getParameter("command"));
        String command = request.getParameter("command");
        boolean forwardingHappened = executeCommandRedirectAfter(request, response);
        if (!forwardingHappened){
            preparePageForwardAfter(request, response);
        }
    }

    private boolean executeCommandRedirectAfter(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        boolean forwardingHappened = false;
        CommandBuilder commandBuilder = CommandBuilder.getInstance();
        Optional<Command> commandOptional = commandBuilder.build(request.getParameter("command"));
        if (commandOptional.isPresent()) {
            try {
                Command command = commandOptional.get();
                Optional<String> optionalUrl = command.execute(request);
                if (optionalUrl.isPresent()) {
                    String url = optionalUrl.get();
                    response.sendRedirect(url);
                    forwardingHappened = true;
                }
            } catch (ControllerException e) {
                LOGGER.error("", e);
                response.sendRedirect(ERROR_PAGE);
                String url = ERROR_PAGE;
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
                forwardingHappened = true;
            } catch (Exception e){
                LOGGER.error("", e);
                String url = ERROR_PAGE;
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
                forwardingHappened = true;
            }
        }
        return forwardingHappened;
    }

    private void preparePageForwardAfter(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        PageBuilder pageBuilder = PageBuilder.getInstance();
        Page page = pageBuilder.build(request.getParameter("page"));
        try {
            String url = page.preparePage(request);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        } catch (ControllerException e){
            LOGGER.error("", e);
            String url = ERROR_PAGE;
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        } catch (Exception e){
            LOGGER.error("", e);
            String url = ERROR_PAGE;
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        }
    }
}
