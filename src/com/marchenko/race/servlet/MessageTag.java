package com.marchenko.race.servlet;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by Marchenko Vadim on 12/22/2015.
 */
@SuppressWarnings("serial")
public class MessageTag extends TagSupport {
    private String typeMessage;
    private String message;
    private String closeText;

    public void setTypeMessage(String typeMessage) {
        this.typeMessage = typeMessage;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCloseText(String closeText) {
        this.closeText = closeText;
    }

    @Override
    public int doStartTag() throws JspTagException {
        JspWriter out = pageContext.getOut();
        try {
            if (!message.isEmpty() && !typeMessage.isEmpty() && !closeText.isEmpty()) {
                out.write("<div id=\"error-message-wrapper\">");
                out.write("<div class=\"black-overlay\" onclick=\"document.getElementById('error-message-wrapper')"
                        + ".style.visibility='hidden'\"></div>");
                out.write("<div id=\"error-message\">");
                out.write("<div class=\"header\">" + typeMessage + "</div>");
                out.write("<div class=\"content\">" + message + "</div>");
                out.write("<div class=\"footer clear-fix\">");
                out.write("<button class=\"btn-primary btn btn-sm\"\n" +
                        "    onclick=\"document.getElementById('error-message-wrapper').style.visibility='hidden'\"\n" +
                        "            >" + closeText + "" +
                        "    </button>");
                out.write("</div>");
                out.write("</div>");
                out.write("</div>");
            }
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }

        return SKIP_BODY;
    }
}
