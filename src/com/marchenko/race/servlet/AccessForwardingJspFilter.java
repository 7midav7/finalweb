package com.marchenko.race.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Marchenko Vadim on 12/22/2015.
 */

@WebFilter(filterName = "AccessForwardingJspFilter", urlPatterns = {"*.jsp"}, dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD,
        DispatcherType.INCLUDE
})
public class AccessForwardingJspFilter implements Filter{
    private static final String SITE = "http://localhost:8088";
    private static final String URL_NOT_ENOUGH_RIGHTS = "/page/log_out_user/access_denied.jsp";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        StringBuffer startUrl = request.getRequestURL();
        String url = startUrl.toString().replace(SITE, "");
        ManagerUrlsWithAccessLevel managerUrlsWithAccessLevel = ManagerUrlsWithAccessLevel.getInstance();
        if (!managerUrlsWithAccessLevel.haveAccessRights(request, url)){
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(URL_NOT_ENOUGH_RIGHTS);
            dispatcher.forward(request, response);
        } else {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
